// @ts-check
/**
 * @license MIT
 * @author Vitalii Vasilev
 */

/**
* @typedef type_sql_column
* @property {string} database
* @property {string} schema
* @property {string} table
* @property {string} column
* @property {string} type
* @property {boolean} nullable
* @property {number} len
* @property {number} precision
* @property {number} scale
* @property {string} default
*/

/**
* @typedef type_sql_script
* @property {string} script
* @property {string} [bulk_name_param]
* @property {string} [bulk_name_table]
*/

/**
* @typedef type_sql_param
* @property {string} name
* @property {string} type
* @property {boolean} nullable
* @property {number} len
* @property {number} precision
* @property {number} scale
* @property {string} [note]
* @property {type_sql_param[]} [sql_param_list]
*/

/**
* @typedef type_sql_result
* @property {string} name
* @property {string} [note]
*/

/**
* @typedef type_action_loaded
* @property {lib_index.type_action} action loaded action
* @property {string} key
* @property {string[]} error_list
* @property {type_sql_script[]} sql_script_list
* @property {type_sql_param[]} sql_param_list
* @property {type_sql_result[]} sql_result_list
* @property {string} sql_lock
* @property {boolean} sql_lock_has_param
* @property {number} sql_lock_wait
* @property {function} js_script
* @property {string[]} keys_next
* @property {string} correction_utc_mode
*/

/**
* @typedef type_link
* @property {string} type
* @property {string} from
* @property {string} to
*/

/** @private */
const lib_os = require('os')
/** @private */
const lib_fs = require('fs')
/** @private */
const lib_path = require('path')
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_utils = require('./utils.js')
/** @private */
const lib_index = require('./index.js')

module.exports = Server_mssql_action

/**
* @class  (license MIT) library for work with Microsoft SQL Server by actions
*/
function Server_mssql_action() {
    if (!(this instanceof Server_mssql_action)) return new Server_mssql_action()
}

/** @type {type_action_loaded[]} */
Server_mssql_action.prototype.list = []

/** @type {type_sql_column[]} */
Server_mssql_action.prototype.column_list = []

/** @type {lib_index} */
Server_mssql_action.prototype.server_mssql

/** @type {type_link[]} */
Server_mssql_action.prototype.link = []

/**
 * add one action
 * @param {lib_index.type_action} action single action
 * @return {string} error that prevented load action
 */
Server_mssql_action.prototype.add = function (action) {
    if (lib_vconv.isAbsent(action)) return 'Action is empty'
    if (lib_vconv.isEmpty(action.key)) return 'Action.key is empty'
    if (lib_vconv.isEmpty(action.sql_script)) return 'Action.sql_script is empty'
    if (this.list.some(f => f.key === action.key.trim().toLowerCase())) return lib_vconv.format('This code (Action.key="{0}") already loaded in actions',action.key)

    /** @type {type_action_loaded} */
    let action_loaded = {
        action: action,
        key: action.key.trim().toLowerCase(),
        error_list: [],
        sql_script_list: [],
        sql_param_list: [],
        sql_result_list: [],
        sql_lock: action.sql_lock,
        sql_lock_wait: lib_vconv.toInt(action.sql_lock_wait, 0),
        sql_lock_has_param: false,
        js_script: undefined,
        keys_next: (lib_vconv.isEmpty(action.keys_next) ? [] : action.keys_next.split(';').map(m => { return m.trim().toLowerCase() }).filter(f => !lib_vconv.isEmpty(f))),
        correction_utc_mode: action.correction_utc_mode
    }

    let error_list = []

    try {
        action_loaded.sql_param_list = parse_sql_param_list(
            action.sql_param,
            action.sql_param_note,
            this.server_mssql.internal_connection_config().database,
            this.column_list
        )
    } catch (error) {
        error_list.push(error.toString())
    }

    try {
        action_loaded.sql_script_list = parse_sql_script_list(action.sql_script, action_loaded.sql_param_list)
    } catch (error) {
        error_list.push(error.toString())
    }

    try {
        action_loaded.sql_result_list = parse_sql_result_list(action.sql_result)
        action_loaded.sql_result_list.filter(f => f.name.substring(0, 1) === '*').forEach(sql_result => {
            this.link.push({type: 'sql_result', from: action_loaded.key, to: sql_result.name.substring(1, sql_result.name.length)})
        })
    } catch (error) {
        error_list.push(error.toString())
    }

    if (!lib_vconv.isEmpty(action.sql_result) && lib_vconv.isEmpty(action.js_script)) {
        action.js_script = 'dataset-to-properties'
    }

    try {
        action_loaded.js_script = parse_js_script(action.js_script)
    } catch (error) {
        error_list.push('error in convert js script string to function - '.concat(error.toString()))
    }

    try {
        if (!lib_vconv.isEmpty(action_loaded.sql_lock)) {
            action_loaded.sql_lock = action_loaded.sql_lock.split(';').map(m=> { return m.trim() }).filter(f => !lib_vconv.isEmpty(f)).join(';').toLowerCase()
        }
        action_loaded.sql_lock_has_param = sql_lock_has_param(action_loaded.sql_lock, action_loaded.sql_param_list)
    } catch (error) {
        error_list.push('error check sql_lock_has_param - '.concat(error.toString()))
    }

    action_loaded.error_list = error_list
    this.list.push(action_loaded)
    resolve_link(this.list, this.link)

    try {
        parse_keys_next(this.list)
    } catch (error) {
        error_list.push('error in parse keys next - '.concat(error.toString()))
    }

    if (error_list.length > 0) {
        return error_list.join(lib_os.EOL)
    } else {
        return undefined
    }
}

/**
 * delete action from list
 * @param {string} key action key
 */
Server_mssql_action.prototype.del = function (key) {
    if (lib_vconv.isEmpty(key)) return
    let i = this.list.findIndex(f => f.key.toLowerCase() === key.toLowerCase())
    if (i >= 0) {
        this.list.splice(i ,1)
    }
}

/**
 * exec action
 * @param {string} key uniq key of action
 * @param {any} params
 * @param {lib_index.callback_action} callback
 */
Server_mssql_action.prototype.exec = function (key, params, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        let action = this.list.find(f => f.key === key.trim().toLowerCase())
        if (lib_vconv.isAbsent(action)) {
            throw new Error (lib_vconv.format('action with key "{0}" not found',key))
        }
        if (!lib_vconv.isAbsent(action.error_list) && action.error_list.length > 0) {
            throw new Error (lib_vconv.format('action with key "{0}" load with error(s): {1}', [action.key, action.error_list.join(', ')]))
        }
        if (!lib_vconv.isAbsent(params) && Array.isArray(params)) {
            throw new Error (lib_vconv.format('in exec action "{0}" params can not be array', action.key))
        }
        if (action.keys_next.length > 0) {
            throw new Error (lib_vconv.format('in action "{0}" find non-parsing keys_next (count = "{1}", first = "{2}")', [action.key, action.keys_next.length, action.keys_next[0]]))
        }

        let sql_lock = (action.sql_lock_has_param === true ? action.sql_lock.split(';') : (lib_vconv.isEmpty(action.sql_lock) ? [] : [action.sql_lock]))
        let script_by_param = []
        action.sql_param_list.forEach(param => {
            let incoming_parameter_value = lib_vconv.findPropertyValueInObject(params, param.name)
            if (lib_vconv.isAbsent(incoming_parameter_value) && !param.nullable) {
                throw new Error(lib_vconv.format('in exec action "{0}" in incoming params not found param with name "{1}"',[action.key, param.name]))
            }

            if (param.type.toLowerCase() === 'table') {
                let s_find = action.sql_script_list.find(f => lib_vconv.toString(f.bulk_name_param,'').toLowerCase() === lib_vconv.toString(param.name,'').toLowerCase())
                if (lib_vconv.isAbsent(s_find)) {
                    throw new Error(lib_vconv.format('in exec action "{0}" not found script for bulk insert for param with name "{1}"',[action.key, param.name]))
                }
                let s = s_find.script
                if (!lib_vconv.isAbsent(incoming_parameter_value)) {
                    if (!Array.isArray(incoming_parameter_value)) {
                        throw new Error(lib_vconv.format('in exec action "{0}" incoming param with name "{1}" must be array',[action.key, param.name]))
                    }
                    if (incoming_parameter_value.length > 0) {
                        s = s.concat(lib_os.EOL, 'INSERT INTO ', s_find.bulk_name_table, '(', param.sql_param_list.map(f => {return lib_vconv.border_add(f.name, '[', ']')}).join(', '),')')
                        incoming_parameter_value.forEach((incoming_row, incoming_row_index) => {
                            s = s.concat(
                                lib_os.EOL,
                                'SELECT ',
                                param.sql_param_list.map(f => {return lib_utils.script_assign_by_type_sql(f.type, lib_vconv.findPropertyValueInObject(incoming_row, f.name))}).join(', '),
                                (incoming_parameter_value.length > incoming_row_index + 1 ? ' UNION ALL' : '')
                            )
                        })
                    }
                }
                script_by_param.push({param: param, script: s})
            } else {
                let s = 'DECLARE @'.concat(param.name, ' ', lib_utils.script_declare_by_type_sql(param.type, param.precision, param.scale, param.len))
                let script_assign_by_type_sql
                if (!lib_vconv.isAbsent(incoming_parameter_value)) {
                    if (Array.isArray(incoming_parameter_value)) {
                        throw new Error(lib_vconv.format('in exec action "{0}" incoming param with name "{1}" should not be array',[action.key, param.name]))
                    }
                    script_assign_by_type_sql = lib_utils.script_assign_by_type_sql(param.type, incoming_parameter_value)
                    s = s.concat('; SET @', param.name, ' = ', script_assign_by_type_sql)
                }
                script_by_param.push({param: param, script: s})

                if (action.sql_lock_has_param === true) {
                    let index_lock_part = sql_lock.findIndex(f => (f.length > 1 && f.substring(0,1) === '@' ? f.substring(1,f.length) : f ) === param.name.toLowerCase())
                    if (index_lock_part >= 0) {
                        sql_lock[index_lock_part] = lib_vconv.replaceAll(script_assign_by_type_sql, "'", "")
                    }
                }
            }
        })

        /** @type {string[]} */
        let sql_script_exec = []

        action.sql_script_list.forEach(sql_script => {
            if (!lib_vconv.isEmpty(sql_script.bulk_name_param)) {
                let f = script_by_param.find(f => lib_vconv.toString(f.param.name,'').toLowerCase() === lib_vconv.toString(sql_script.bulk_name_param,'').toLowerCase())
                if (!lib_vconv.isAbsent(f)) {
                    sql_script_exec.push(f.script)
                }
            } else {
                let s_l = sql_script.script.toLowerCase()
                let script_prefix = ''
                script_by_param.filter(f => f.param.type.toLowerCase() !== 'table').forEach(s_b_p => {
                    if (s_l.indexOf('@'.concat(s_b_p.param.name.toLowerCase())) >= 0) {
                        script_prefix = script_prefix.concat(s_b_p.script, lib_os.EOL)
                    }
                })
                script_prefix = script_prefix.concat(sql_script.script)
                sql_script_exec.push(script_prefix)
            }
        })

        /** @private @type {lib_index.type_exec_option} */
        let options = {}

        if (sql_lock.length > 0) {
            options.lock = {
                key: sql_lock.join(";"),
                wait: action.action.sql_lock_wait,
                message: action.action.sql_lock_message
            }
        }

        this.server_mssql.exec(sql_script_exec, options, exec_result => {

            if (!lib_vconv.isAbsent(exec_result.handle_error.error)) {
                callback(exec_result, undefined, undefined)
            } else {
                let named_tables
                let warning
                if (!lib_vconv.isAbsent(action.js_script) && !lib_vconv.isAbsent(exec_result.tables)) {
                    let parse = action.js_script(exec_result.tables, action.sql_result_list)
                    named_tables = parse.data
                    warning = parse.warning
                }

                callback(exec_result, named_tables, warning)
            }
        })
    } catch (error) {
        callback({
            handle_error: {
                error: error,
                point: 'action',
                step: undefined
            },
            performance: {
                duration_exec_millisecond: [],
                perfomance_exec_after: undefined,
                perfomance_exec_before: undefined
            }
        }, undefined, undefined)
    }
}

/**
 * generate text for js-file with functions for exec action
 * @param {lib_index.type_generate_class_with_exec_actions} [param]
 * @returns {string}
 */
Server_mssql_action.prototype.generate_class_with_exec_actions = function (param) {
    let _get_property_typedef = function(function_name, params) {
        let result = []
        if (lib_vconv.isAbsent(params)) return result

        params.forEach(param => {
            let type = 'any'
            if (param.type.toLowerCase() === 'table') {
                if (lib_vconv.isEmpty(function_name)) {
                    type = 'any['
                } else {
                    type = 'typetable_'.concat(function_name,'_',param.name,'[]')
                }
            } else {
                let type_sql = lib_utils.types_sql().find(f => f.type.toLowerCase() === param.type.toLowerCase())
                if (!lib_vconv.isAbsent(type_sql) && !lib_vconv.isEmpty(type_sql.jstype)) {
                    type = type_sql.jstype
                }
            }

            let name = param.name
            if (param.nullable === true) {
                name = '['.concat(param.name, ']')
            }

            result.push({name: name, type: type, note: param.note})
        })

        return result
    }

    if (lib_vconv.isAbsent(param)) param = {}

    param.action_class_template = lib_vconv.toString(param.action_class_template, lib_fs.readFileSync(lib_path.join(__dirname, 'action_class_template.txt'), 'utf8'))
    param.class_name = lib_vconv.toString(param.class_name, 'ExecMsSqlActions')

    let template_class = ''
    let template_action = ''
    let functions_action = []

    let prev_text = ''
    param.action_class_template.split('==============================').forEach(text => {
        text = text.trim()
        if (!lib_vconv.isEmpty(text)) {
            if (prev_text === '==========template class') {
                template_class = text
            } else if (prev_text === '==========template action') {
                template_action = text
            }
        }
        prev_text = text
    })

    this.list.forEach(action => {
        let function_name = lib_vconv.replaceAll(action.key, '-', '_')

        let typedefs = []
        let function_params = 'callback'
        let exec_action_params = 'undefined'
        let param_action_params = '%%DELETE-LINE%%'

        action.sql_param_list.filter(f => f.type.toLowerCase() === 'table' && !lib_vconv.isAbsent(f.sql_param_list) && f.sql_param_list.length > 0).forEach(param => {
            typedefs.push({
                name: 'typetable_'.concat(function_name,'_',param.name),
                property: _get_property_typedef(undefined, param.sql_param_list)
            })
        })
        if (action.sql_param_list.length > 0) {
            typedefs.push({
                name: 'typeparam_'.concat(function_name),
                property: _get_property_typedef(function_name, action.sql_param_list)
            })
            param_action_params = ' * @param {typeparam_'.concat(function_name,'} [action_params]')
            function_params = 'action_params, callback'
            exec_action_params = 'action_params'
        }
        if (action.sql_result_list.length > 0) {
            typedefs.push({
                name: 'typecallback_'.concat(function_name),
                property: action.sql_result_list.map(m => { return {name: m.name, type: 'Object[]', note: m.note} })
            })
        }
        let typedef = typedefs.map(m => {
            return ''.concat(
                '/**', lib_os.EOL,
                ' * @private', lib_os.EOL,
                ' * @typedef ', m.name, lib_os.EOL,
                m.property.map(mm => { return ' * @property '.concat('{', mm.type, '} ', mm.name, ' ', lib_vconv.toString(mm.note, '')).trimRight()}).join(lib_os.EOL),
                lib_os.EOL,
                ' */'
            )
        }).join(lib_os.EOL)

        let type_callback = ''
        let param_callback = ' * @param {callback_error} [callback]'
        let result_to_object = 'result'
        if (action.sql_result_list.length > 0) {
            type_callback = ''.concat(
                '/**', lib_os.EOL,
                ' * @private', lib_os.EOL,
                ' * @callback callback_', function_name, lib_os.EOL,
                ' * @param {string} error_text', lib_os.EOL,
                ' * @param {typecallback_',function_name,'} [result]', lib_os.EOL,
                ' */'
            )
            param_callback = ''.concat(' * @param {callback_',function_name,'} [callback]')
            result_to_object = ''.concat(
                '{',
                action.sql_result_list.map(m => { return ''.concat(
                    lib_vconv.format('{0} : (is_empty(result.data.{0}) ? [] : result.data.{0})', m.name),
                    )}).join(', '),
                '}',
            )
        }

        let function_action = template_action
        function_action = lib_vconv.replaceAll(function_action, '%action_name%', action.key)
        function_action = lib_vconv.replaceAll(function_action, '%action_title%', (lib_vconv.isEmpty(action.action.title) ? '%%DELETE-LINE%%' : ' * '.concat(action.action.title)))
        function_action = lib_vconv.replaceAll(function_action, '%function_name%', function_name)
        function_action = lib_vconv.replaceAll(function_action, '%class_name%', param.class_name)
        function_action = lib_vconv.replaceAll(function_action, '%typedef%', typedef)
        function_action = lib_vconv.replaceAll(function_action, '%typecallback%', type_callback)
        function_action = lib_vconv.replaceAll(function_action, '%function_action_params_jsdoc%', param_action_params)
        function_action = lib_vconv.replaceAll(function_action, '%function_callback_param_jsdoc%', param_callback)
        function_action = lib_vconv.replaceAll(function_action, '%function_params%', function_params)
        function_action = lib_vconv.replaceAll(function_action, '%exec_action_params%', exec_action_params)
        function_action = lib_vconv.replaceAll(function_action, '%result_to_object%', result_to_object)

        functions_action.push(function_action)
    })

    let text_prepare = lib_vconv.replaceAll(template_class, '%class_name%', param.class_name)
    text_prepare = lib_vconv.replaceAll(text_prepare, '%actions%', functions_action.join(lib_os.EOL))

    let text = []
    text_prepare.split(lib_os.EOL).forEach(line => {
        if (line.trim() !== '%%DELETE-LINE%%') {
            text.push(lib_vconv.replaceAll(line, '%%DELETE-LINE%%', ''))
        }
    })
    return text.join(lib_os.EOL)
}

/**
 * @private
 * @param {string} sql_param
 * @param {string} sql_param_note
 * @param {string} connection_config_database_name
 * @param {type_sql_column[]} column_list
 * @returns {type_sql_param[]}
 */
function parse_sql_param_list (sql_param, sql_param_note, connection_config_database_name, column_list) {
    /** @type {type_sql_param[]} */
    let result = []

    if (lib_vconv.isEmpty(sql_param)) return result
    sql_param = sql_param.trim()

    // split params by single param OR table param
    let cnt_left_breaked = 0
    let param_list = []
    let param_text = ''
    for (let i = 0; i < sql_param.length; i++) {
        let ch = sql_param[i]

        if (ch === ';' && cnt_left_breaked === 0) {
            param_list.push(param_text)
            param_text = ''
        } else {
            if (ch === '{') {
                cnt_left_breaked++
            } else if (ch === '}') {
                cnt_left_breaked--
                if (cnt_left_breaked < 0) {
                    throw new Error ('incorrect number of brackets "}" relative to brackets "{" in sql param')
                }
            }
            param_text = param_text.concat(ch)
        }
    }
    if (cnt_left_breaked > 0) {
        throw new Error ('incorrect number of brackets "}" relative to brackets "{" in sql param')
    }
    param_text = param_text.trim()
    if (!lib_vconv.isEmpty(param_text)) {
        param_list.push(param_text)
    }

    // parse param
    param_list.forEach(param => {
        let param_worlds =  param.trim().split(' ').filter(f => !lib_vconv.isEmpty(f));
        if (param_worlds.length < 1) {
            throw new Error (lib_vconv.format('not found param name and type in string "{0}"',param))
        }

        // param name
        let param_name = param_worlds[0].trim()
        if (param_name.length > 1 && param_name.substring(0,1) === '##') {
            param_name = param_name.substring(2, param_name.length)
        } else if (param_name.length > 0 && ['#','@'].includes(param_name.substring(0,1))) {
            param_name = param_name.substring(1, param_name.length)
        }
        param_name = lib_vconv.border_del(param_name,'[',']')
        if (lib_vconv.isEmpty(param_name)) {
            throw new Error (lib_vconv.format('not found param name in param "{0}"',param))
        }
        param_worlds.shift()

        // param nullable
        let param_nullable = true

        if (param_worlds.length > 0 && param_worlds[param_worlds.length-1].trim().toLowerCase() === 'null') {
            param_worlds.pop()
            if (param_worlds.length > 0 && param_worlds[param_worlds.length-1].trim().toLowerCase() === 'not') {
                param_worlds.pop()
                param_nullable = false
            }
        }

        if (param_name.substring(0,1) === '*') {
            // get params from regular table
            let field_to_max = false
            param_name = param_name.substring(1,param_name.length)
            if (param_name.length > 0 && param_name.substring(0,1) === '*') {
                param_name = param_name.substring(1,param_name.length)
                field_to_max = true
            }
            if (lib_vconv.isEmpty(param_name)) {
                throw new Error (lib_vconv.format('in param name found "*" but regular table name not found'))
            }
            let table_name = lib_utils.parse_table_name(param_name)
            if (lib_vconv.isEmpty(table_name.database)) {
                table_name.database = connection_config_database_name
            }
            let table_find = column_list.filter(f => f.database.toLowerCase() === table_name.database.toLowerCase() && f.schema.toLowerCase() === table_name.schema.toLowerCase() && f.table.toLowerCase() === table_name.table.toLowerCase())
            if (table_find.length <= 0) {
                throw new Error (lib_vconv.format('not found table "{0}.{1}.{2}" in loaded table and column list',[table_name.database, table_name.schema, table_name.table]))
            }

            table_find.forEach(column => {
                let type = column.type
                let len = column.len
                if (type.toLowerCase() === 'timestamp') {
                    type = 'binary'
                    len = 8
                } else {
                    if (field_to_max === true) {
                        let fnd_types_sql = lib_utils.types_sql().find(f => f.type === column.type.toLowerCase())
                        if (!lib_vconv.isAbsent(fnd_types_sql) && fnd_types_sql.len === 'allow') {
                            len = -1
                        }
                    }
                }

                result.push({
                    name: column.column,
                    type: type,
                    nullable: (field_to_max === true ? true : column.nullable),
                    len: len,
                    precision: column.precision,
                    scale: column.scale,
                    note: undefined,
                    sql_param_list: []
                })
            })
        } else {
            // param child params (for table), type, len, scale, precision
            if (param_worlds.length <= 0) {
                throw new Error (lib_vconv.format('in param "{0}" type not found',param))
            }

            let param_type
            let param_len
            let param_precision
            let param_scale
            let sql_param_list = []
            if (param_worlds[0].toLowerCase() === 'table' || param_worlds[0].substring(0,6).toLowerCase() === 'table{') {
                let param_type_dirty = param_worlds.join(' ')
                let i_1 = param_type_dirty.indexOf('{')
                if (i_1 < 0) {
                    throw new Error (lib_vconv.format('substring "table" find, but substring "{" not find in param "{0}"',param_type_dirty))
                }
                let i_2 = param_type_dirty.lastIndexOf('}')
                if (i_2 < 0) {
                    throw new Error (lib_vconv.format('substring "table" find, but substring "}" not find in param "{0}"',param_type_dirty))
                }

                sql_param_list = parse_sql_param_list(param_type_dirty.substring(i_1 + 1, i_2), undefined, connection_config_database_name, column_list)
                param_type = 'table'
            } else {
                let param_type_dirty = param_worlds.join('')
                let position_bracket_open = param_type_dirty.indexOf('(')
                let position_bracket_close = param_type_dirty.indexOf(')')
                if (position_bracket_open >=0 && position_bracket_close < 0) {
                    throw new Error (lib_vconv.format('in type "{0}" found bracket "(", but ")" not found',param_type_dirty))
                } else if (position_bracket_open < 0 && position_bracket_close >= 0) {
                    throw new Error (lib_vconv.format('in type "{0}" found bracket ")", but "(" not found',param_type_dirty))
                } else if (position_bracket_open >=0 && position_bracket_close>=0 && position_bracket_open > position_bracket_close) {
                    throw new Error (lib_vconv.format('in type "{0}" bracket ")" found before "("',param_type_dirty))
                } else if (position_bracket_close>=0 && position_bracket_close + 1 != param_type_dirty.length) {
                    throw new Error (lib_vconv.format('in type "{0}" unknown ending "{1}"',[param_type_dirty, param_type_dirty.substring(position_bracket_close + 1, param_type_dirty.length)]))
                }
                if (position_bracket_open < 0) {
                    param_type = param_type_dirty
                } else {
                    param_type = param_type_dirty.substring(0,position_bracket_open)

                    if (param_type_dirty.substring(position_bracket_open,position_bracket_close + 1).toLowerCase() === '(max)') {
                        param_len = -1
                    } else {
                        let position_comma = param_type_dirty.indexOf(',')
                        if (position_comma < 0) {
                            param_len = lib_vconv.toInt(param_type_dirty.substring(position_bracket_open + 1,position_bracket_close))
                        } else {
                            param_precision = lib_vconv.toInt(param_type_dirty.substring(position_bracket_open + 1, position_comma))
                            param_scale = lib_vconv.toInt(param_type_dirty.substring(position_comma + 1, param_type_dirty.length - 1))
                        }
                    }
                }
                param_type = lib_vconv.border_del(param_type,'[',']')
                if (param_type.toLowerCase() === 'guid') {
                    param_type = 'uniqueidentifier'
                }
            }

            result.push({
                name: param_name,
                type: param_type,
                nullable: param_nullable,
                len: param_len,
                precision: param_precision,
                scale: param_scale,
                note: undefined,
                sql_param_list: sql_param_list
            })
        }
    })

    // remove dublicate param
    for (let i = 0; i < result.length; i++) {
        for (let j = i + 1; j < result.length; j++) {
            if (result[i].name.toLowerCase() === result[j].name.toLowerCase()) {
                result.splice(i, 1)
                break;
            }
        }
    }

    // set note param
    if (!lib_vconv.isEmpty(sql_param_note)) {
        sql_param_note.split(';').filter(f => !lib_vconv.isEmpty(f)).forEach(note_dirty => {
            note_dirty = note_dirty.trim()
            let position_space = note_dirty.indexOf(' ')
            if (position_space > 0) {
                let param_note = note_dirty.substring(position_space, note_dirty.length).trim()
                if (!lib_vconv.isEmpty(param_note)) {
                    let param_name = note_dirty.substring(0, position_space).trim()
                    if (param_name.length > 1 && param_name.substring(0,1) === '##') {
                        param_name = param_name.substring(2, param_name.length)
                    } else if (param_name.length > 0 && ['#','@'].includes(param_name.substring(0,1))) {
                        param_name = param_name.substring(1, param_name.length)
                    }
                    param_name = lib_vconv.border_del(param_name,'[',']')

                    let fnd_param = result.find(f => f.name.toLowerCase() === param_name.toLowerCase())
                    if (!lib_vconv.isAbsent(fnd_param)) {
                        fnd_param.note = param_note
                    }
                }
            }
        })
    }

    // check param
    result.forEach(param => {
        if (param.type.toLowerCase() === 'table') {
            if (lib_vconv.isAbsent(param.sql_param_list) || param.sql_param_list.length <= 0) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" not found columns',[param.name, param.type]))
            }
        } else {
            let fnd_types_sql = lib_utils.types_sql().find(f => f.type === param.type.toLowerCase())
            if (lib_vconv.isAbsent(fnd_types_sql)) {
                throw new Error (lib_vconv.format('in param with name "{0}" unknown type "{1}"',[param.name, param.type]))
            }
            // precision
            if (typeof fnd_types_sql.precision === 'number') {
                param.precision = fnd_types_sql.precision
            } else if (fnd_types_sql.precision === 'allow' && lib_vconv.toInt(param.precision, -1) <= 0) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" not found precision',[param.name, param.type]))
            } else if (fnd_types_sql.precision === 'deny' && !lib_vconv.isAbsent(param.precision)) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" found precision "{2}"',[param.name, param.type, param.precision]))
            }
            // scale
            if (typeof fnd_types_sql.scale === 'number') {
                param.scale = fnd_types_sql.scale
            } else if (fnd_types_sql.scale === 'allow' && lib_vconv.toInt(param.scale, -1) < 0) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" not found scale',[param.name, param.type]))
            } else if (fnd_types_sql.scale === 'deny' && !lib_vconv.isAbsent(param.scale)) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" found scale "{2}"',[param.name, param.type, param.scale]))
            }
            // len
            if (typeof fnd_types_sql.len === 'number') {
                param.len = fnd_types_sql.len
            } else if (fnd_types_sql.len === 'allow') {
                let len = lib_vconv.toInt(param.len, -2)
                if (len !== -1 && len < 0) {
                    throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" not found length',[param.name, param.type]))
                }
            } else if (fnd_types_sql.len === 'allow_no_max') {
                if (param.len.toString() === '-1') {
                    throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" length cannot be "max"',[param.name, param.type]))
                }
                if (lib_vconv.toInt(param.len, -1) < 0) {
                    throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" not found length',[param.name, param.type]))
                }
            } else if (fnd_types_sql.len === 'deny' && !lib_vconv.isAbsent(param.len)) {
                throw new Error (lib_vconv.format('in param with name "{0}" and type "{1}" found length "{2}"',[param.name, param.type, param.len]))
            }
        }
    })

    return result
}

/**
 * @private
 * @param {type_action_loaded[]} action_loaded
 */
function parse_keys_next (action_loaded) {
    let resolved_keys_next = false
    action_loaded.filter(f => f.keys_next.length > 0).forEach(action => {
        /** @type {type_action_loaded[]} */
        let actions_parent = []
        action.keys_next.forEach(key => {
            let fnd_parent = action_loaded.find(f => f.key === key && f.key !== action.key && f.keys_next.length === 0)
            if (!lib_vconv.isAbsent(fnd_parent)) {
                actions_parent.push(fnd_parent)
            }
        })
        if (actions_parent.length === action.keys_next.length) {
            actions_parent.forEach(action_parent => {
                let sql_script_list = ''
                action_parent.sql_script_list.forEach((sql, idx) => {
                    sql_script_list = sql_script_list.concat(
                        lib_os.EOL,
                        lib_vconv.format('/* STEP #{0} FROM action {1} */', [idx,action_parent.key]),
                        lib_os.EOL,
                        sql.script
                    )
                })
                action.sql_script_list[action.sql_script_list.length - 1].script = action.sql_script_list[action.sql_script_list.length - 1].script.concat(sql_script_list)
                action_parent.sql_result_list.forEach(result => {
                    if (action.sql_result_list.some(f => f.name === result.name)) {
                        throw new Error (lib_vconv.format('in child and parent actions find sql_result with same name = "{0}"',result.name))
                    }
                    action.sql_result_list.push(result)
                })
            })

            if (!lib_vconv.isEmpty(action.sql_result_list) && lib_vconv.isEmpty(action.js_script)) {
                action.js_script = parse_js_script('dataset-to-properties')
            }
            action.keys_next = []
            resolved_keys_next = true
        }
    })
    if (resolved_keys_next) {
        parse_keys_next(action_loaded)
    }
}

/**
 * @private
 * @param {string} sql_script
 * @param {type_sql_param[]} sql_param_list
 * @returns {type_sql_script[]}
 */
function parse_sql_script_list(sql_script, sql_param_list) {
    /** @type {type_sql_script[]} */
    let sql_script_list = []

    if (lib_vconv.isEmpty(sql_script)) return sql_script_list

    let index_bulk_start = sql_script.indexOf('{bulk ')
    while (index_bulk_start >= 0) {
        let index_bulk_stop = sql_script.indexOf('}', index_bulk_start) + 1

        let sql_before = sql_script.substring(0, index_bulk_start).trim()
        if (!lib_vconv.isEmpty(sql_before)) {
            sql_script_list.push({script: sql_before})
        }

        let sql_bulk = sql_script.substring(index_bulk_start, index_bulk_stop).trim()
        let sql_bulk_name_table = sql_bulk.substring(5, sql_bulk.length - 1).trim()
        let sql_bulk_name_param = undefined
        let param_index = sql_bulk_name_table.indexOf('=')
        if (param_index >=0) {
            sql_bulk_name_param = sql_bulk_name_table.substring(param_index + 1, sql_bulk_name_table.length).trim()
            sql_bulk_name_table = sql_bulk_name_table.substring(0, param_index).trim()
        } else {
            sql_bulk_name_param = sql_bulk_name_table
        }
        sql_bulk_name_param = lib_vconv.border_del(sql_bulk_name_param,'#',undefined)
        sql_bulk_name_param = lib_vconv.border_del(sql_bulk_name_param,'#',undefined)

        let fnd_param_with_columns = sql_param_list.find(f => f.name.toLowerCase() === sql_bulk_name_param.toLowerCase())
        if (lib_vconv.isAbsent(fnd_param_with_columns)) {
            throw new Error (lib_vconv.format('not found columns for table with name {0}',sql_bulk_name_param))
        }

        let script =  ''.concat(lib_vconv.format("IF OBJECT_ID('{0}') IS NOT NULL DROP TABLE {1}",[
            (sql_bulk_name_table.substring(0,1) === '#' ? 'tempdb..'.concat(sql_bulk_name_table) : sql_bulk_name_table),
            sql_bulk_name_table
        ]))

        script = script.concat(lib_os.EOL, lib_vconv.format('CREATE TABLE {0} (',sql_bulk_name_table))
        fnd_param_with_columns.sql_param_list.forEach((column,index) => {
            if (index !== 0) {
                script = script.concat(', ')
            }
            script = script.concat(lib_vconv.border_add(column.name,'[',']'),' ',lib_utils.script_declare_by_type_sql(column.type, column.precision, column.scale, column.len))
            if (column.nullable === false) {
                script = script.concat(' NOT NULL')
            }
        })
        script = script.concat(')')

        sql_script_list.push({script: script, bulk_name_table: sql_bulk_name_table, bulk_name_param: sql_bulk_name_param})

        sql_script = sql_script.substring(index_bulk_stop, sql_script.length).trim()
        index_bulk_start = sql_script.indexOf('{bulk ')
    }
    if (!lib_vconv.isEmpty(sql_script)) {
        sql_script_list.push({script: sql_script})
    }

    return sql_script_list
}

/**
 * @private
 * @param {string} sql_result
 * @returns {type_sql_result[]}
 */
function parse_sql_result_list(sql_result) {
    /** @type {type_sql_result[]} */
    let sql_result_list = []

    if (lib_vconv.isEmpty(sql_result)) return sql_result_list

    sql_result.split(';').filter(f => !lib_vconv.isEmpty(f)).forEach(note_dirty => {
        note_dirty = note_dirty.trim()
        let param_name = note_dirty
        let param_note = undefined
        let position_space = note_dirty.indexOf(' ')
        if (position_space > 0) {
            param_name = note_dirty.substring(0, position_space).trim()
            param_note = note_dirty.substring(position_space, note_dirty.length).trim()
        }
        if (param_name.length > 1 && param_name.substring(0,1) === '##') {
            param_name = param_name.substring(2, param_name.length)
        } else if (param_name.length > 0 && ['#','@'].includes(param_name.substring(0,1))) {
            param_name = param_name.substring(1, param_name.length)
        }
        param_name = lib_vconv.border_del(param_name,'[',']')
        if (lib_vconv.isEmpty(param_name)) {
            throw new Error ('name of one of the sql result is empty')
        }
        sql_result_list.push({
            name: param_name,
            note: param_note
        })
    })

    let check = check_dublicate_result(sql_result_list)
    if (!lib_vconv.isEmpty(check)) {
        throw new Error (check)
    }

    return sql_result_list
}

/**
 * @private
 * @param {string} js_script
 * @returns {function}
 */
function parse_js_script(js_script) {
    if (lib_vconv.isEmpty(js_script)) return undefined
    if (js_script.toLowerCase() === 'dataset-to-properties') {
        return ((tables, sql_result_list) => {
            if (lib_vconv.isAbsent(tables)) {
                tables = []
            }
            if (lib_vconv.isAbsent(sql_result_list)) {
                sql_result_list = []
            }
            let data = {}
            let schema = {}
            tables.forEach((table, index) => {
                let property_name = (sql_result_list.length > index ? sql_result_list[index].name : 'undefined'.concat(index))
                data[property_name] = table.rows
                schema[property_name] = table.schema
            })
            let warning = []
            if (tables.length !== sql_result_list.length) {
                warning.push(lib_vconv.format("the number of tables ({0}) and their descriptions ({1}) does not match", [tables.length, sql_result_list.length]))
            }
            return {data: data, schema: schema, warning: warning}
        })
    } else if (js_script.toLowerCase() === 'dataset-to-properties-desc') {
        return ((tables, sql_result_list) => {
            if (lib_vconv.isAbsent(tables)) {
                tables = []
            }
            if (lib_vconv.isAbsent(sql_result_list)) {
                sql_result_list = []
            }
            let data = {}
            let schema = {}
            tables.reverse().forEach((table, index) => {
                let property_name = (sql_result_list.length > index ? sql_result_list[index].name : 'undefined'.concat(index))
                data[property_name] = table.rows
                schema[property_name] = table.schema
            })
            let warning = []
            if (tables.length !== sql_result_list.length) {
                warning.push(lib_vconv.format("the number of tables ({0}) and their descriptions ({1}) does not match", [tables.length, sql_result_list.length]))
            }
            return {data: data, schema: schema, warning: warning}
        })
    } else {
        return new Function(js_script)
    }
}

/**
 * @param {type_action_loaded[]} actions
 * @param {type_link[]} links
 */
function resolve_link (actions, links) {
    if (lib_vconv.isAbsent(links) || links.length <= 0) return
    let cnt_resolve = 0
    for (let i = 0; i < links.length; i++) {
        if (links[i].type === 'sql_result') {
            let action_to = actions.find(f => f.key.toLowerCase() === links[i].to.toLowerCase())
            if (lib_vconv.isAbsent(action_to)) continue
            if (action_to.sql_result_list.some(f => f.name.substring(0,1) === '*')) continue
            let action_from = actions.find(f => f.key.toLowerCase() === links[i].from.toLowerCase())
            if (lib_vconv.isAbsent(action_from)) continue
            let action_from_result_index = action_from.sql_result_list.findIndex(f => f.name.toLowerCase() === '*'.concat(action_to.key.toLowerCase()))
            if (action_from_result_index >= 0) {
                action_from.sql_result_list.splice(action_from_result_index, 1, ...action_to.sql_result_list)
                links.splice(i, 1)
                let check = check_dublicate_result(action_from.sql_result_list)
                if (!lib_vconv.isEmpty(check)) {
                    action_from.error_list.push(check)
                }
                cnt_resolve++
            }
        }
    }
    if (cnt_resolve > 0) {
        resolve_link(actions, links)
    }
}

/**
 * @param {type_sql_result[]} sql_result_list
 * @returns {string}
 */
function check_dublicate_result(sql_result_list) {
    for (let i = 0; i < sql_result_list.length; i++) {
        for (let j = i + 1; j < sql_result_list.length; j++) {
            if (sql_result_list[i].name.toLowerCase() === sql_result_list[j].name.toLowerCase()) {
                return lib_vconv.format('sql result with name "{0}" occurs more than once',sql_result_list[i].name)
            }
        }
    }
}

/**
 * @param {string} sql_lock
 * @param {type_sql_param[]} sql_param_list
 * @returns {boolean}
 */
function sql_lock_has_param (sql_lock, sql_param_list) {
    if (lib_vconv.isEmpty(sql_lock)) return false
    if (sql_param_list.length <= 0) return false
    let sql_lock_split = sql_lock.split(';')
    for (let i = 0; i < sql_param_list.length; i++) {
        if (sql_param_list[i].type === 'table') continue
        for (let j = 0; j < sql_lock_split.length; j++) {
            let sel_lock_part = sql_lock_split[j]
            if (sel_lock_part.length > 1 && sel_lock_part.substring(0,1) === '@') sel_lock_part = sel_lock_part.substring(1, sel_lock_part.length)
            if (sql_param_list[i].name.toLowerCase() === sel_lock_part) {
                return true
            }
        }
    }
    return false
}