// @ts-check

// @ts-ignore
const assert = require('assert');
// @ts-ignore
const testing_lib = require('./../index.js')()
const testing_lib_utils = require('./../utils.js')

describe("coming soon", function() {
})

describe("utils.parse_table_name", function() {
    it('parse "mybase.[myschema].mytable" to {mybase, myschema, mytable}', function() {
        assert.deepEqual(testing_lib_utils.parse_table_name("mybase.[myschema].mytable"), {database: 'mybase', schema: 'myschema', table: 'mytable'})
    })
    it('parse "[myschema].mytable" to {undefined, myschema, mytable}', function() {
        assert.deepEqual(testing_lib_utils.parse_table_name("[myschema].mytable"), {database: undefined, schema: 'myschema', table: 'mytable'})
    })
    it('parse "mytable" to {undefined, dbo, mytable}', function() {
        assert.deepEqual(testing_lib_utils.parse_table_name("mytable"), {database: undefined, schema: 'dbo', table: 'mytable'})
    })
    it('parse "mybase..mytable" to {mybase, dbo, mytable}', function() {
        assert.deepEqual(testing_lib_utils.parse_table_name("mybase..mytable"), {database: 'mybase', schema: 'dbo', table: 'mytable'})
    })
    it('parse ".mytable" to {undefined, dbo, mytable}', function() {
        assert.deepEqual(testing_lib_utils.parse_table_name(".mytable"), {database: undefined, schema: 'dbo', table: 'mytable'})
    })
})