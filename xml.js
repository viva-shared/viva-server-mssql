// @ts-check
/**
 * write result to XML file
 * @license MIT
 * @author Vitalii vasilev
 */

 /** @private */
 const lib_fs = require('fs')
 /** @private */
const lib_os = require('os')
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_util = require('./utils.js')

exports.write = write

/**
 * @callback callback_write
 * @param {Error} error
 * @param {string} xml  //only if input param full_file_name is empty
 *//**
 * @param {string} [full_file_name] //if empty, see xml as a string in callback
 * @param {Object[]} table_list
 * @param {string[]} table_name_list
 * @param {callback_write} callback
 */

function write (full_file_name, table_list, table_name_list, callback) {
    if (lib_vconv.isEmpty(full_file_name)) {
        /** @type {string[]} */
        let xml = []
        try {
            write_start(undefined, xml)
            write_xsd(undefined, xml, table_list, table_name_list)
            write_data(undefined, xml, table_list, table_name_list)
            write_stop(undefined, xml)
            xml.push('')
            callback(undefined, xml.join(lib_os.EOL))
        } catch (error) {
            xml = undefined
            callback(error, undefined)
        }
    } else {
        let stream
        lib_fs.access(full_file_name, lib_fs.constants.F_OK, error => {
            try {
                if (lib_vconv.isAbsent(error)) lib_fs.unlinkSync(full_file_name)
                stream = lib_fs.createWriteStream(full_file_name)

                stream.on('error', error => {
                    callback(error, undefined)
                    stream = undefined
                })

                stream.once('ready', () => {
                    try {
                        write_start(stream, undefined)
                        write_xsd(stream, undefined, table_list, table_name_list)
                        write_data(stream, undefined, table_list, table_name_list)
                        write_stop(stream, undefined)

                        if (!lib_vconv.isAbsent(stream)) {
                            stream.end()
                            stream = undefined
                        }
                        callback(undefined, undefined)
                    } catch (error) {
                        callback(error, undefined)
                    }
                })
            } catch (error) {
                stream = undefined
                callback(error, undefined)
                return
            }
        })
    }
}

/**
 * @param {NodeJS.WritableStream} stream
 * @param {string[]} xml
 * @param {string} text
 */
function write_core (stream, xml, text) {
    if (!lib_vconv.isAbsent(stream)) {
        stream.write(lib_vconv.toString(text,'').concat(lib_os.EOL))
    }
    if (!lib_vconv.isAbsent(xml)) {
        xml.push(lib_vconv.toString(text,''))
    }
}

/**
 * @param {NodeJS.WritableStream} stream
 * @param {string[]} xml
 */
function write_start (stream,  xml) {
    write_core(stream, xml, '<?xml version="1.0" standalone="yes"?>')
    write_core(stream, xml, '<data>')
}

/**
 * @param {NodeJS.WritableStream} stream
 * @param {string[]} xml
 */
function write_stop (stream, xml) {
    write_core(stream, xml, '</data>')
}

/**
 * @param {NodeJS.WritableStream} stream
 * @param {string[]} xml
 * @param {Object[]} table_list
 * @param {string[]} table_name_list
 */
function write_xsd (stream, xml, table_list, table_name_list) {
    let open_xs_schema
    if (!lib_vconv.isAbsent(table_list) && table_list.length > 0) {
        table_list.forEach((table, table_index) => {
            if (!lib_vconv.isAbsent(table) && !lib_vconv.isAbsent(table.schema)) {
                let table_name = get_table_name(table_name_list, table_index)
                let schema = get_schema(table.schema)
                for (let i = 0; i < schema.length; i++) {
                    for (let j = i + 1; j < schema.length; j++) {
                        if (schema[i].name_xml === schema[j].name_xml) {
                            let error_text = lib_vconv.format('in table "{0}" field "{1}" occurs more than once', [table_name, schema[i].name_xml])
                            let field_replace = []
                            if (schema[i].prop_name !== schema[i].name_xml) field_replace.push(schema[i].prop_name)
                            if (schema[j].prop_name !== schema[j].name_xml) field_replace.push(schema[j].prop_name)
                            if (field_replace.length > 0) {
                                error_text = lib_vconv.format('{0} (field name obtained by replacing prohibited characters from the field(s) {1}{2}{1})', [error_text, '"', field_replace.join('" and "')])
                            }
                            throw new Error(error_text)
                        }
                    }
                }

                if (open_xs_schema !== true) {
                    write_core(stream, xml, '<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">')
                    open_xs_schema = true
                }

                write_core(stream, xml, lib_vconv.format('<xs:element name="{0}"><xs:complexType><xs:sequence>', table_name))
                schema.forEach(col => {
                    write_core(stream, xml, lib_vconv.format('<xs:element name="{0}" type="{1}"/>', [col.name_xml, col.type_xml]))
                })
                write_core(stream, xml, '</xs:sequence></xs:complexType></xs:element>')
            }
        })
    }
    if (open_xs_schema === true) {
        write_core(stream, xml, '</xs:schema>')
    }
}

/**
 * @param {NodeJS.WritableStream} stream
 * @param {string[]} xml
 * @param {Object[]} table_list
 * @param {string[]} table_name_list
 */
function write_data (stream, xml, table_list, table_name_list) {
    if (!lib_vconv.isAbsent(table_list) && table_list.length > 0) {
        table_list.forEach((table, table_index) => {
            if (!lib_vconv.isAbsent(table) && !lib_vconv.isAbsent(table.schema) && !lib_vconv.isAbsent(table.rows)) {
                let table_name = get_table_name(table_name_list, table_index)
                let row_name_start =''.concat('<', table_name, '>')
                let row_name_stop = ''.concat('</', table_name, '>')
                let schema = get_schema(table.schema)
                table.rows.forEach(row => {
                    write_core(stream, xml, row_name_start)

                    schema.forEach(col => {
                        let row_value = lib_vconv.findPropertyValueInObject(row, col.prop_name)
                        if (!lib_vconv.isAbsent(row_value) && Array.isArray(row_value)) {
                            throw new Error(lib_vconv.format('in table "{0}" field "{1}" occurs more than once', [table_name, col.prop_name]))
                        }
                        let cell_line = get_cell_line(col, row_value)
                        if (!lib_vconv.isEmpty(cell_line)) {
                            write_core(stream, xml, cell_line)
                        }
                    })
                    write_core(stream, xml, row_name_stop)
                })
            }
        })
    }
}

/**
 * @param {any} text
 * @returns {string}
 */
function w_esc (text) {
    let t = text
    t = lib_vconv.replaceAll(t, '&', '&amp;')
    t = lib_vconv.replaceAll(t, '<', '&lt;')
    t = lib_vconv.replaceAll(t, '>', '&gt;')
    t = lib_vconv.replaceAll(t, '"', '&quot;')
    t = lib_vconv.replaceAll(t, "'", '&apos;')
    return t
}

/**
 * @param {string[]} table_name_list
 * @param {number} index
 * @returns {string}
 */
function get_table_name(table_name_list, index) {
    if (lib_vconv.isAbsent(table_name_list) || !Array.isArray(table_name_list) || table_name_list.length <= index) return '_table_'.concat(index.toString())
    return w_esc(table_name_list[index])
}

/**
 * @param {type_get_schema} col
 * @param {string} cell_value
 * @returns {string}
 */
function get_cell_line(col, cell_value) {
    if (lib_vconv.isAbsent(col)) return ''
    let format_cell_value = cell_value

    if (['xs:date', 'xs:dateTime', 'xs:time'].includes(col.type_xml)) {
        format_cell_value = lib_vconv.formatDate(cell_value, 126)
    } else if (['xs:boolean'].includes(col.type_xml)) {
        let b = lib_vconv.toBool(cell_value)
        if (b === true) {
            format_cell_value = 'true'
        } else if (b === false) {
            format_cell_value = 'false'
        }
    } else if (['xs:decimal'].includes(col.type_xml)) {
        let f = lib_vconv.toFloat(cell_value)
        if (!lib_vconv.isAbsent(f)) {
            format_cell_value = f.toString()
        }
    } else if (['xs:integer'].includes(col.type_xml)) {
        let i = lib_vconv.toInt(cell_value)
        if (!lib_vconv.isAbsent(i)) {
            format_cell_value = i.toString()
        }
    } else {
        format_cell_value = w_esc(lib_vconv.toString(cell_value, ''))
    }

    if (lib_vconv.isEmpty(format_cell_value)) {
        return ''
    }

    return lib_vconv.format('<{0}>{1}</{0}>', [col.name_xml, format_cell_value])
}

/**
 * @typedef type_get_schema
 * @property {string} prop_name
 * @property {string} type_sql
 * @property {string} type_js
 * @property {string} type_xml
 * @property {string} name_xml
 *//**
 * @param {Object} schema
 * @returns {type_get_schema[]}
 */
function get_schema(schema) {
    if (lib_vconv.isAbsent(schema)) return []
    /** @private @type {type_get_schema[]} */
    let result = []
    let types_sql = lib_util.types_sql()

    for (let prop_name in schema) {
        let type_sql = undefined
        let type_js = 'string'
        let type_xml = 'xs:string'

        let prop = lib_vconv.findPropertyValueInObject(schema, prop_name)
        if (!lib_vconv.isAbsent(prop) && !lib_vconv.isAbsent(prop.type)) {
            let fnd_type = types_sql.find(f => f.type === prop.type.declaration)
            if (!lib_vconv.isAbsent(fnd_type) && !lib_vconv.isEmpty(fnd_type.xsdtype)) {
                type_sql = fnd_type.type
                type_js = fnd_type.jstype
                type_xml = fnd_type.xsdtype
            }
        }

        let name_xml = prop_name
        ' {}[]():;!?@#$%^&*=+|,\'\"<>~`№\\/'.split('').forEach(bad_symb => {
            name_xml = lib_vconv.replaceAll(name_xml, bad_symb, '_')
        })

        result.push({
            prop_name: prop_name,
            type_sql: type_sql,
            type_js: type_js,
            type_xml: type_xml,
            name_xml: name_xml
        })
    }

    return result
}