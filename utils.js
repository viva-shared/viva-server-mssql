// @ts-check
/**
 * shared small functions for MS SQL
 * @license MIT
 * @author Vitalii vasilev
 */

/** @private */
const lib_vconv = require('viva-convert')

exports.parse_table_name=parse_table_name
exports.types_sql=types_sql
exports.script_declare_by_type_sql=script_declare_by_type_sql
exports.script_assign_by_type_sql=script_assign_by_type_sql

/**
 * @readonly
 * @typedef type_util_parse_table_name
 * @property {string} database
 * @property {string} schema
 * @property {string} table
 *
 * parse string with table name to 3 substring - database, schema, table
 * @static
 * @param {string} text
 * @return {type_util_parse_table_name}
 */
function parse_table_name (text) {
    if (lib_vconv.isEmpty(text)) return undefined
    let s = text.split('.')
    if (s.length <= 0) {
        return undefined
    }
    if (s.length === 1) {
        return {
            database: undefined,
            schema: 'dbo',
            table: lib_vconv.border_del(s[0].trim(),'[',']')
        }
    }
    if (s.length === 2) {
        let schema = lib_vconv.border_del(s[0].trim(),'[',']')
        return {
            database: undefined,
            schema: (lib_vconv.isEmpty(schema) ? 'dbo' : schema),
            table: lib_vconv.border_del(s[1].trim(),'[',']')
        }
    }
    if (s.length === 3) {
        let schema = lib_vconv.border_del(s[1].trim(),'[',']')
        return {
            database: lib_vconv.border_del(s[0].trim(),'[',']'),
            schema: (lib_vconv.isEmpty(schema) ? 'dbo' : schema),
            table: lib_vconv.border_del(s[2].trim(),'[',']')
        }
    }
    throw new Error (lib_vconv.format('bad string "{0}" for parse to database name, schema name and table name', text))
}

/**
 * @readonly
 * @typedef type_types_sql
 * @property {string} type
 * @property {'deny' | 'allow' | number} precision number or 'deny' or 'allow'
 * @property {'deny' | 'allow' | number} scale number or 'deny' or 'allow'
 * @property {'deny' | 'allow' | 'allow_no_max' | number} len number or 'deny' or 'allow' or 'allow_no_max'
 * @property {'number' | 'Date' | 'string'} jstype
 * @property {'xs:integer' | 'xs:decimal' | 'xs:boolean' | 'xs:date' | 'xs:dateTime' | 'xs:time' | 'xs:string'} xsdtype
 * @property {number} bytes_on_char
 *
 * list sql types with rules
 * @static
 * @return {type_types_sql[]}
 */
function types_sql () {
    return [
        //Exact numerics
        {type: 'bigint',            precision: 19,              scale: 0,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:integer',  bytes_on_char: undefined},
        {type: 'bit',               precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'number',           xsdtype: 'xs:boolean',  bytes_on_char: undefined},
        {type: 'decimal',           precision: 'allow',         scale: 'allow',         len: 'deny',            jstype: 'number',           xsdtype: 'xs:decimal',  bytes_on_char: undefined},
        {type: 'int',               precision: 10,              scale: 0,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:integer',  bytes_on_char: undefined},
        {type: 'money',             precision: 19,              scale: 4,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:decimal',  bytes_on_char: undefined},
        {type: 'numeric',           precision: 'allow',         scale: 'allow',         len: 'deny',            jstype: 'number',           xsdtype: 'xs:decimal',  bytes_on_char: undefined},
        {type: 'smallint',          precision: 5,               scale: 0,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:integer',  bytes_on_char: undefined},
        {type: 'smallmoney',        precision: 10,              scale: 4,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:integer',  bytes_on_char: undefined},
        {type: 'tinyint',           precision: 3,               scale: 0,               len: 'deny',            jstype: 'number',           xsdtype: 'xs:integer',  bytes_on_char: undefined},
        //Approximate numerics
        {type: 'float',             precision: 53,              scale: 'deny',          len: 'deny',            jstype: 'number',           xsdtype: 'xs:decimal',  bytes_on_char: undefined},
        {type: 'real',              precision: 24,              scale: 'deny',          len: 'deny',            jstype: 'number',           xsdtype: 'xs:decimal',  bytes_on_char: undefined},
        //Date and time
        {type: 'date',              precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:date',     bytes_on_char: undefined},
        {type: 'datetime2',         precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:dateTime', bytes_on_char: undefined},
        {type: 'datetime',          precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:dateTime', bytes_on_char: undefined},
        {type: 'datetimeoffset',    precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:string',   bytes_on_char: undefined},
        {type: 'smalldatetime',     precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:dateTime', bytes_on_char: undefined},
        {type: 'time',              precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'Date',             xsdtype: 'xs:time',     bytes_on_char: undefined},
        //Character strings
        {type: 'char',              precision: 'deny',          scale: 'deny',          len: 'allow_no_max',    jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 2},
        {type: 'text',              precision: 'deny',          scale: 'deny',          len: 2147483647,        jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 1},
        {type: 'varchar',           precision: 'deny',          scale: 'deny',          len: 'allow',           jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 1},
        {type: 'sysname',           precision: 'deny',          scale: 'deny',          len: 128,               jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 1},
        //Unicode character strings
        {type: 'nchar',             precision: 'deny',          scale: 'deny',          len: 'allow_no_max',    jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 2},
        {type: 'ntext',             precision: 'deny',          scale: 'deny',          len: 1073741823,        jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 2},
        {type: 'nvarchar',          precision: 'deny',          scale: 'deny',          len: 'allow',           jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: 2},
        //Binary strings
        {type: 'binary',            precision: 'deny',          scale: 'deny',          len: 'allow_no_max',    jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'image',             precision: 'deny',          scale: 'deny',          len: 2147483647,        jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'varbinary',         precision: 'deny',          scale: 'deny',          len: 'allow',           jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        //Other data types
        {type: 'hierarchyid',       precision: 'deny',          scale: 'deny',          len: 892,               jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'sql_variant',       precision: 'deny',          scale: 'deny',          len: 0,                 jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'xml',               precision: 'deny',          scale: 'deny',          len: -1,                jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'uniqueidentifier',  precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: 'string',           xsdtype: 'xs:string',   bytes_on_char: undefined},
        {type: 'timestamp',         precision: 'deny',          scale: 'deny',          len: 'deny',            jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'geometry',          precision: 'deny',          scale: 'deny',          len: -1,                jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined},
        {type: 'geography',         precision: 'deny',          scale: 'deny',          len: -1,                jstype: undefined,          xsdtype: undefined,     bytes_on_char: undefined}
    ]
}

/**
 * generate declaration script with sql type
 * @static
 * @param {string} type
 * @param {string | number} precision
 * @param {string | number} scale
 * @param {string | number} len
 * @returns {string}
 */
function script_declare_by_type_sql (type, precision, scale, len) {
    if (lib_vconv.isEmpty(type)) return undefined
    if (type.toLowerCase() === 'guid') {
        type = 'uniqueidentifier'
    } else if (type.toLowerCase() === 'udt') {
        type = 'hierarchyid'
    } else if (type.toLowerCase() === 'variant') {
        type = 'sql_variant'
    }

    let type_sql = types_sql().find(f => f.type === type.toLowerCase())
    if (lib_vconv.isAbsent(type_sql)) {
        throw new Error(lib_vconv.format('not found sql type "{0}"',type))
    }
    if (
        (type_sql.precision === 'deny' && type_sql.scale === 'deny' && type_sql.len === 'deny') ||
        (typeof type_sql.precision === 'number' && typeof type_sql.scale === 'number' && type_sql.len === 'deny') ||
        (typeof type_sql.precision === 'number' && type_sql.scale === 'deny' && type_sql.len === 'deny') ||
        (type_sql.precision === 'deny' && type_sql.scale === 'deny' && type_sql.len === -1) ||
        (type_sql.precision === 'deny' && type_sql.scale === 'deny' && typeof type_sql.len === 'number' && type_sql.len >= 0)
    ) {
        return type_sql.type.toUpperCase()
    }
    if (type_sql.precision === 'allow' && type_sql.scale === 'allow' && type_sql.len === 'deny') {
        let precision_parse = lib_vconv.toInt(precision, -1)
        if (precision_parse <= 0) {
            throw new Error(lib_vconv.format('not found precision for sql type "{0}"',type_sql.type))
        }
        let scale_parse = lib_vconv.toInt(scale, -1)
        if (scale_parse < 0) {
            throw new Error(lib_vconv.format('not found scale for sql type "{0}"',type_sql.type))
        }
        return type_sql.type.toUpperCase().concat('(',precision_parse.toString(),',',scale_parse.toString(),')')
    }
    if (type_sql.precision === 'deny' && type_sql.scale === 'deny' && type_sql.len === 'allow') {
        let len_parse = lib_vconv.toInt(len, -2)
        if (len_parse <= -2) {
            throw new Error(lib_vconv.format('not found length for sql type "{0}"',type_sql.type))
        }
        if (len_parse === -1) {
            return type_sql.type.toUpperCase().concat('(MAX)')
        } else {
            return type_sql.type.toUpperCase().concat('(',len_parse.toString(),')')
        }
    }
    if (type_sql.precision === 'deny' && type_sql.scale === 'deny' && type_sql.len === 'allow_no_max') {
        let len_parse = lib_vconv.toInt(len, -2)
        if (len_parse <= -2) {
            throw new Error(lib_vconv.format('not found length for sql type "{0}"',type_sql.type))
        }
        if (len === -1) {
            throw new Error(lib_vconv.format('length for sql type "{0}" can not be "MAX"',type_sql.type))
        }
        return type_sql.type.toUpperCase().concat('(',len_parse.toString(),')')
    }
    throw new Error(lib_vconv.format('unknown error in builder sql declaration text from sql type "{0}"',type_sql.type))
}

/**
 * generate declaration script with sql type
 * @static
 * @param {string} type
 * @param {any} value
 * @returns {string}
 */
function script_assign_by_type_sql (type, value) {
    if (lib_vconv.isEmpty(type)) return undefined
    if (lib_vconv.isAbsent(value)) {
        return 'NULL'
    }
    if (lib_vconv.isEmpty(value) && !['char','nvarchar','nchar','sysname','varchar','xml'].includes(type.toLowerCase())) {
        return 'NULL';
    }

    if (type.toLowerCase() === 'guid') {
        type = 'uniqueidentifier'
    }

    switch(type.toLowerCase()) {
        case 'bigint':
        case 'int':
        case 'smallint':
        case 'tinyint':
            let _int = lib_vconv.toInt(value);
            if (!lib_vconv.isEmpty(_int)) {
                return _int.toString();
            }
            throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
        case 'binary':
        case 'varbinary':
        case 'timestamp':
            if (value.type === 'Buffer') {
                if (!lib_vconv.isAbsent(value.data) && Array.isArray(value.data)) {
                    return '0x'.concat(Buffer.from(value.data).toString('hex'));
                }
            }
            if (value.toLowerCase() === '0x') {
                return '0x';
            }
            let _binary = lib_vconv.toInt(value);
            if (!lib_vconv.isAbsent(_binary)) {
                return '0x'+_binary;
            }
            if (value.substring(0,1) === '0' && value.substring(1,2).toLowerCase() === 'x' && value.length > 2) {
                var binary_bad = false;
                for (let binary_i = 2; binary_i < value.length; binary_i++) {
                    if (!['0','1','2','3','4','5','6','7','8','9','0','a','b','c','d','e','f'].includes(value[binary_i].toLowerCase())) {
                        binary_bad = true;
                        break;
                    }
                }
                if (!binary_bad) {
                    return '0x'+value.substring(2,value.length).toUpperCase();
                }
            }
            throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
        case 'bit':
            let maybe_bit = lib_vconv.toBool(value);
            if (maybe_bit === true) {
                return '1';
            }
            if (maybe_bit === false) {
                return '0';
            }
            throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
        case 'char':
        case 'sysname':
        case 'varchar':
            return "'" + lib_vconv.replaceAll(value,"'","''") + "'";
        case 'nvarchar':
        case 'nchar':
        case 'xml':
            return "N'" + lib_vconv.replaceAll(value,"'","''") + "'";
        case 'date':
        case 'datetime':
        case 'datetime2':
        case 'smalldatetime':
        case 'datetimeoffset':
            let _date = lib_vconv.toDate(value);
            if (!lib_vconv.isEmpty(_date)) {
                let get_month = _date.getMonth() + 1;
                if (type.toLowerCase() === 'date' ||
                    (_date.getHours() === 0 && _date.getMinutes() === 0 && _date.getSeconds() === 0 && _date.getMilliseconds() === 0)) {
                    return "'".concat(_date.getFullYear().toString(),
                        (get_month <= 9 ? '0': ''), get_month.toString(),
                        (_date.getDate() <= 9 ? '0': ''), _date.getDate().toString(),"'");
                }
                return "'".concat(_date.getFullYear().toString(), '-',
                (get_month <= 9 ? '0': ''), get_month.toString(), '-',
                (_date.getDate() <= 9 ? '0': ''), _date.getDate().toString(), 'T',
                (_date.getHours() <= 9 ? '0': ''), _date.getHours().toString(), ':',
                (_date.getMinutes() <= 9 ? '0': ''), _date.getMinutes().toString(), ':',
                (_date.getSeconds() <= 9 ? '0': ''), _date.getSeconds().toString(), '.',
                (_date.getMilliseconds() >= 100 ? '': (_date.getMilliseconds() >= 10 ? '0' : '00')), _date.getMilliseconds().toString(),
                "'"
                );
            }
            throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
        case 'decimal':
        case 'float':
        case 'money':
        case 'numeric':
        case 'real':
        case 'smallmoney':
            let _number = lib_vconv.toFloat(value);
            if (lib_vconv.isEmpty(_number)) {
                throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
            }
            return _number.toString();
        case 'sql_variant':
            return value;
        case 'time':
            let _time = lib_vconv.toTime(value);
            if (!lib_vconv.isEmpty(_time)) {
                return "'".concat((_time.getHours() <= 9 ? '0': ''), _time.getHours().toString(), ':',
                (_time.getMinutes() <= 9 ? '0': ''), _time.getMinutes().toString(), ':',
                (_time.getSeconds() <= 9 ? '0': ''), _time.getSeconds().toString(), '.',
                (_time.getMilliseconds() > 100 ? '': (_time.getMilliseconds() > 10 ? '0' : '00')), _time.getMilliseconds().toString(),
                "'"
                );
            }
            throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
        case 'uniqueidentifier':
            let _uniqueidentifier = lib_vconv.toGuid(value);
            if (lib_vconv.isEmpty(_uniqueidentifier)) {
                throw new Error(lib_vconv.format('failed convert value "{0}" to sql type "{1}"',[value,type]))
            }
            return "'" + _uniqueidentifier + "'";
        case 'geography':
        case 'geometry':
        case 'hierarchyid':
            throw new Error(lib_vconv.format('sql type "{0}" not supported',[type]));
        case 'image':
        case 'ntext':
        case 'text':
            throw new Error(lib_vconv.format('sql type "{0}" not supported and not planned in the future',[type]));
        default:
            throw new Error(lib_vconv.format('unknown sql type "{0}"',[type]));
    }
}