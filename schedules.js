// @ts-check
/**
 * @license MIT
 * @author Vitalii Vasilev
 */

/**
* @typedef type_job_internal_props
* @property {Server_mssql_schedule} self
* @property {'waiting'|'working'|'need_delete'|'allow_delete'} state
* @property {number} count_work
* @property {Date} last_time_work
* @property {type_job} [job_for_update]
* @property {lib_schedule.Job} [schedule]
* @property {NodeJS.Timer} [timeout]
*/

/**
* @typedef type_job
* @property {string} key job uniq key
* @property {string|function} sql_script script or function return string - script
* @property {'daily_once'|'daily_every'} type
* @property {Object} [external_params]
* @property {type_job_internal_props} [_private]
* @property {type_shedule_daily_once} [shedule_daily_once]
* @property {type_shedule_daily_every} [shedule_daily_every]
* @property {Function} [callback_result] function (result)
*/

/**
* @typedef type_shedule_daily_once
* @property {number} h
* @property {number} m
* @property {number} s
*/

/**
* @typedef type_shedule_daily_every
* @property {number} time
* @property {'h'|'m'|'s'} type 'h' (hour), 'm' (minute), 's' (second)
* @property {number} h1
* @property {number} m1
* @property {number} s1
* @property {number} h2
* @property {number} m2
* @property {number} s2
*/

/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_index = require('./index.js')
/** @private */
const lib_schedule = require('node-schedule')

module.exports = Server_mssql_schedule

lib_util.inherits(Server_mssql_schedule, lib_event)
Server_mssql_schedule.prototype.emit = Server_mssql_schedule.prototype.emit || undefined
Server_mssql_schedule.prototype.on = Server_mssql_schedule.prototype.on || undefined

/** @type {lib_index} */
Server_mssql_schedule.prototype.server_mssql = undefined
/** @type {type_job[]} */
Server_mssql_schedule.prototype.jobs = []

/**
* @class  (license MIT) library for work with Microsoft SQL Server by schedule
*/
function Server_mssql_schedule() {
    if (!(this instanceof Server_mssql_schedule)) return new Server_mssql_schedule()
    lib_event.call(this)
}

/**
 * @param {type_job} job
 */
Server_mssql_schedule.prototype.go = function (job) {
    if (lib_vconv.isAbsent(job)) return

    if (job.type === 'daily_once') {
        if (lib_vconv.isAbsent(job.shedule_daily_once)) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" shedule_daily_once is empty',[job.key, job.type]))
        }
        if (lib_vconv.toInt(job.shedule_daily_once.h, -1) < 0 || lib_vconv.toInt(job.shedule_daily_once.h, -1) > 23) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.h int between 0 and 23, but present "{2}"',[job.key, job.type, job.shedule_daily_once.h]))
        }
        if (lib_vconv.toInt(job.shedule_daily_once.m, -1) < 0 || lib_vconv.toInt(job.shedule_daily_once.m, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.m int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_once.m]))
        }
        if (lib_vconv.toInt(job.shedule_daily_once.s, -1) < 0 || lib_vconv.toInt(job.shedule_daily_once.s, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.s int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_once.s]))
        }

        /** @type {type_job} */
        let new_job = {
            key: job.key,
            sql_script: job.sql_script,
            type: job.type,
            external_params: job.external_params,
            callback_result: job.callback_result,
            _private : {
                self: this,
                state: 'waiting',
                count_work: 0,
                last_time_work: undefined
            },
            shedule_daily_once: {
                h: job.shedule_daily_once.h,
                m: job.shedule_daily_once.m,
                s: job.shedule_daily_once.s
            }
        }

        let exists_job = this.jobs.find(f => f.key === new_job.key)

        if (lib_vconv.isAbsent(exists_job)) {

            this.jobs.push(new_job)
            emit(this, new_job, undefined, 'adding')
            new_job._private.schedule = lib_schedule.scheduleJob(
                lib_vconv.format('{0} {1} {2} * * *',[new_job.shedule_daily_once.s, new_job.shedule_daily_once.m, new_job.shedule_daily_once.h]),
                function() {
                    if (new_job._private.state === 'waiting') {
                        new_job._private.state = 'working'
                        new_job._private.last_time_work = new Date()
                        emit(new_job._private.self, new_job, undefined, 'starting')
                        let sql_script = ''
                        if (typeof new_job.sql_script === 'function') {
                            sql_script = new_job.sql_script()
                        } else {
                            sql_script = lib_vconv.toString(new_job.sql_script)
                        }
                        new_job._private.self.server_mssql.exec(sql_script, undefined, exec_result => {
                            new_job._private.count_work++
                            if (['need_delete','allow_delete'].includes(new_job._private.state)) {
                                new_job._private.state = 'allow_delete'
                                emit(new_job._private.self, new_job, exec_result, 'stopped')
                                new_job._private.self.delete(new_job.key)
                            } else {
                                new_job._private.state = 'waiting'
                                emit(new_job._private.self, new_job, exec_result, 'stopped')
                            }
                        })
                    }
                }
            )

        } else if (exists_job.type !== new_job.type ||
            exists_job.sql_script !== new_job.sql_script ||
            exists_job.shedule_daily_once.h !== new_job.shedule_daily_once.h ||
            exists_job.shedule_daily_once.m !== new_job.shedule_daily_once.m ||
            exists_job.shedule_daily_once.s !== new_job.shedule_daily_once.s) {

            exists_job._private.job_for_update = new_job
            this.delete(exists_job.key)

        }
    } else if (job.type === 'daily_every') {
        if (lib_vconv.isAbsent(job.shedule_daily_every)) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" shedule_daily_every is empty',[job.key, job.type]))
        }
        if (!['h','m','s'].includes(job.shedule_daily_every.type)) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" unknown shedule_daily_every.type "{2}"',[job.key, job.type, job.shedule_daily_every.type]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.time, 0) <= 0) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.time int more than 0, but present "{2}"',[job.key, job.type, job.shedule_daily_every.time]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.h1, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.h1, -1) > 23) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.h1 int between 0 and 23, but present "{2}"',[job.key, job.type, job.shedule_daily_every.h1]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.m1, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.m1, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.m1 int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_every.m1]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.s1, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.s1, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.s1 int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_every.s1]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.h2, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.h2, -1) > 23) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.h2 int between 0 and 23, but present "{2}"',[job.key, job.type, job.shedule_daily_every.h2]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.m2, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.m2, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.m2 int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_every.m2]))
        }
        if (lib_vconv.toInt(job.shedule_daily_every.s2, -1) < 0 || lib_vconv.toInt(job.shedule_daily_every.s2, -1) > 59) {
            throw new Error(lib_vconv.format('in job with key "{0}" and type "{1}" expected shedule_daily_once.s2 int between 0 and 59, but present "{2}"',[job.key, job.type, job.shedule_daily_every.s2]))
        }

        /** @type {type_job} */
        let new_job = {
            key: job.key,
            sql_script: job.sql_script,
            type: job.type,
            external_params: job.external_params,
            callback_result: job.callback_result,
            _private : {
                self: this,
                state: 'waiting',
                count_work: 0,
                last_time_work: undefined
            },
            shedule_daily_every: {
                type:job.shedule_daily_every.type,
                time:job.shedule_daily_every.time,
                h1:job.shedule_daily_every.h1,
                m1:job.shedule_daily_every.m1,
                s1:job.shedule_daily_every.s1,
                h2:job.shedule_daily_every.h2,
                m2:job.shedule_daily_every.m2,
                s2:job.shedule_daily_every.s2
            }
        }

        let exists_job = this.jobs.find(f => f.key === new_job.key)

        if (lib_vconv.isAbsent(exists_job)) {

            this.jobs.push(new_job)
            emit(this, new_job, undefined, 'adding')

            let step = new_job.shedule_daily_every.time
            if (new_job.shedule_daily_every.type === 's') {
                step = step * 1000
            } else if (new_job.shedule_daily_every.type === 'm') {
                step = step * 1000 * 60
            } else if (new_job.shedule_daily_every.type === 'h') {
                step = step * 1000 * 60 * 60
            }
            let now = new Date()
            let noon = new Date(now.getFullYear(), now.getMonth(), now.getDate())
            let not_used = Math.ceil((now.getTime() - noon.getTime()) / step)
            let first_day_step = (noon.getTime() + (not_used * step)) - now.getTime()

            new_job._private.timeout = setTimeout(function tick() {
                let now = new Date()
                let d = new Date(2000, 0, 1, now.getHours(), now.getMinutes(), now.getSeconds()).getTime()
                let d1 = new Date(2000, 0, 1, new_job.shedule_daily_every.h1, new_job.shedule_daily_every.m1, new_job.shedule_daily_every.s1).getTime()
                let d2 = new Date(2000, 0, 1, new_job.shedule_daily_every.h2, new_job.shedule_daily_every.m2, new_job.shedule_daily_every.s2).getTime()

                if (new_job._private.state === 'waiting' && d1 <= d && d <= d2) {
                    new_job._private.state = 'working'
                    new_job._private.last_time_work = new Date()
                    emit(new_job._private.self, new_job, undefined, 'starting')
                    let sql_script = ''
                    if (typeof new_job.sql_script === 'function') {
                        sql_script = new_job.sql_script()
                    } else {
                        sql_script = lib_vconv.toString(new_job.sql_script)
                    }
                    new_job._private.self.server_mssql.exec(sql_script, undefined, exec_result => {
                        new_job._private.count_work++
                        if (['need_delete','allow_delete'].includes(new_job._private.state)) {
                            new_job._private.state = 'allow_delete'
                            emit(new_job._private.self, new_job, exec_result, 'stopped')
                            new_job._private.self.delete(new_job.key)
                        } else {
                            new_job._private.state = 'waiting'
                            emit(new_job._private.self, new_job, exec_result, 'stopped')
                        }
                        new_job._private.timeout = setTimeout(tick, step)
                    })
                } else {
                    new_job._private.timeout = setTimeout(tick, step)
                }
            }, first_day_step)
        } else if (exists_job.type !== new_job.type ||
            exists_job.sql_script !== new_job.sql_script ||
            exists_job.shedule_daily_every.time !== new_job.shedule_daily_every.time ||
            exists_job.shedule_daily_every.type !== new_job.shedule_daily_every.type ||
            exists_job.shedule_daily_every.h1 !== new_job.shedule_daily_every.h1 ||
            exists_job.shedule_daily_every.m1 !== new_job.shedule_daily_every.m1 ||
            exists_job.shedule_daily_every.s1 !== new_job.shedule_daily_every.s1 ||
            exists_job.shedule_daily_every.h2 !== new_job.shedule_daily_every.h2 ||
            exists_job.shedule_daily_every.m2 !== new_job.shedule_daily_every.m2 ||
            exists_job.shedule_daily_every.s2 !== new_job.shedule_daily_every.s2) {

            exists_job._private.job_for_update = new_job
            this.delete(exists_job.key)

        }

    } else {
        throw new Error('unknown job.type='.concat(job.type))
    }
}

/**
 * @param {string} job_key
 */
Server_mssql_schedule.prototype.delete = function (job_key) {
    let index_job = this.jobs.findIndex(f => f.key === job_key)
    if (index_job < 0) return
    if (this.jobs[index_job].type === 'daily_once') {
        if (['waiting','allow_delete'].includes(this.jobs[index_job]._private.state)) {
            this.jobs[index_job]._private.schedule.cancel()
            this.jobs[index_job]._private.schedule = undefined
            emit(this, this.jobs[index_job], undefined, 'deleted')
            let updated_job = this.jobs[index_job]._private.job_for_update
            this.jobs.splice(index_job, 1)
            if (!lib_vconv.isAbsent(updated_job)) {
                this.go(updated_job)
            }
        } else {
            this.jobs[index_job]._private.state = 'need_delete'
            emit(this, this.jobs[index_job], undefined, 'need-delete')
        }
    } else if (this.jobs[index_job].type === 'daily_every') {
        if (['waiting','allow_delete'].includes(this.jobs[index_job]._private.state)) {
            clearTimeout(this.jobs[index_job]._private.timeout)
            this.jobs[index_job]._private.timeout = undefined
            emit(this, this.jobs[index_job], undefined, 'deleted')
            let updated_job = this.jobs[index_job]._private.job_for_update
            this.jobs.splice(index_job, 1)
            if (!lib_vconv.isAbsent(updated_job)) {
                this.go(updated_job)
            }
        } else {
            this.jobs[index_job]._private.state = 'need_delete'
            emit(this, this.jobs[index_job], undefined, 'need-delete')
        }
    }
}

/**
 * @param {Server_mssql_schedule} self
 * @param {type_job} job
 * @param {lib_index.type_exec_result} exec_result
 * @param {Object} log_action
 */
function emit (self, job, exec_result, log_action) {

    if (!lib_vconv.isAbsent(exec_result)) {
        if (!lib_vconv.isAbsent(exec_result.handle_error.error)) {
            self.emit('error', job, exec_result)
        } else {
            self.emit('result', job, exec_result)
            if (lib_vconv.isFunction(job.callback_result)) {
                try {
                    job.callback_result(exec_result)
                } catch (error) {
                }
            }
        }
    }

    if (!lib_vconv.isAbsent(log_action)) {
        self.emit('log', job, log_action)
    }
}