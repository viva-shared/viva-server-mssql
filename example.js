// @ts-check

let lib_sql = require('viva-server-mssql')
let sql = new lib_sql()
sql.init({
    server: 'myserver\\myinstance',
    login: 'sa',
    password: 'mypassword',
    again_connect_count: 5,
    again_connect_timeout: 1000,
    database: 'mybase'
}, error => {
    if (error) {
        console.log('error init', error)
        return
    }

    //=======================================
    //=====EXEC SCRIPT SUBSYSTEM
    //=======================================
    sql.exec(['select top 10 * from dbo.mytable1; select top 1 * from myotherbase.dbo.mytable2', 'select top 5 * from dbo.mytable3'], undefined, exec_result => {
        if (exec_result.handle_error.error) {
            console.log('ERROR EXEC')
            console.log(exec_result.handle_error)
        } else {
            console.log(exec_result)
        }
    })

    //=======================================
    //=====JOB SUBSYSTEM
    //=======================================
    sql.on('schedule-error', (job, error) => {
        console.log(job)
        console.log(error)
    })
    sql.on('schedule-result', (job, result) => {
        console.log(job)
        console.log(result)
    })
    sql.on('schedule-log', (job, log) => {
        console.log(job)
        console.log(log)
    })
    sql.schedule_go({key: 'job1', sql_script: "SELECT TOP 10 * FROM mytable1", type: 'daily_once', shedule_daily_once: {h: 11, m: 58, s: 0}})
    sql.schedule_go({key: 'job2', sql_script: "SELECT TOP 10 * FROM mytable1", type: 'daily_every', shedule_daily_every: {type: 'm', time: 1, h1: 15, m1: 35, s1: 0, h2: 23, m2: 59, s2: 59}})
})