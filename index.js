// @ts-check
/**
 * @license MIT
 * @author Vitalii Vasilev
 */

/**
* @typedef type_connection_config
* @property {string} server MS SQL instance, for example - 'server\\instance1'
* @property {string} password passrord for ms sql authentication or windows authentication
* @property {string} [login] login for ms sql authentication, if empty - os authentication
* @property {string} database name database for connect, default - 'tempdb'
* @property {string} [app_name] app name, which will be visible in profiler
* @property {number} [connection_timeout] connection timeout in milliseconds, default - 15000
* @property {number} [execution_timeout] execution timeout in milliseconds, default - 0 (infinity)
* @property {boolean} [encrypt_connection] encrypt connection, default - false
* @property {boolean} [useUTC] default - true
* @property {boolean} [action_allow] use action subsystem, default - false
* @property {number} [again_connect_count] count to try to connect to the server if the connection is unavailable, default - 0 (again try connect is disabled)
* @property {number} [again_connect_timeout] timeout in milliseconds between attempts to connect to the server, default - 2000
*/

/**
* @private
* @typedef type_internal_connection_config
* @property {string} server
* @property {string} database
* @property {string} user
* @property {string} password
* @property {string} domain
* @property {number} connectionTimeout
* @property {number} requestTimeout
* @property {any} pool
* @property {Object} options
*/

/**
* @typedef type_action
* @property {string} key action uniq key
* @property {string} sql_script
* @property {string} [sql_param]
* @property {string} [sql_param_note]
* @property {string} [sql_result]
* @property {string} [sql_lock] one thread - name
* @property {number} [sql_lock_wait] one thread - wait timeout in milliseconds (0 - no wait), default 0
* @property {string} [sql_lock_message] one thread - message if lock exists
* @property {string} [js_script]
* @property {string} title action title
* @property {string} [note] action title
* @property {string} [keys_next] exec this actions after exec this action, example - 'key1;key2'
* @property {string} [correction_utc_mode]
* @property {Object} [extented_property]
*/

/**
 * @typedef type_generate_class_with_exec_actions
 * @property {string} [action_class_template]
 * @property {string} [class_name]
 */

/**
 * @callback callback_empty
 */

/**
 * @callback callback_error
 * @param {Error} error
 */

/**
* @typedef type_exec_option
* @property {type_exec_correction_utc_option} [correction_utc]
* @property {type_exec_lock_option} [lock] one thread subsystem
* @property {boolean} [noname_row_beautify] default - false
* @property {number} [chunk_rows] return result by chunk, default - off (undefined)
* @property {number} [chunk_msec] return result by chunk, default - off (undefined)
* @property {boolean} [check_database_after_exec] return database name, which is active at the time the script is finished, default - false
* @property {boolean} [check_spid] return spid, for (example) kill process, default - false
* @property {boolean} [check_print] return PRINT and RAISERROR, default - false
* @property {boolean} [type_ext] create properties "type_name", "type_title", "type_charlen" in schema
*/

/**
 * @typedef type_exec_lock_option
 * @property {string} [key]
 * @property {number} [wait]
 * @property {string} [message]
 */

/**
 * @typedef type_exec_lock_result
 * @property {string} [key]
 * @property {number} [wait]
 * @property {string} [message]
 * @property {boolean} allow
 */

/**
 * @typedef type_exec_correction_utc_option
 * @property {number} [offset] correct field with datetime type by this value (in minutes)
 * @property {string} [mode] which fields correct by utc offset, example - '{#all}{}{fdm;ldm}' - in first table correct all datetime fields, second table is missing, in the third table correct only two fields
 */

/**
 * @typedef type_exec_correction_utc_result
 * @property {number} [offset] correct field with datetime type by this value (in minutes)
 * @property {string} [mode] which fields correct by utc offset, example - '{#all}{}{fdm;ldm}' - in first table correct all datetime fields, second table is missing, in the third table correct only two fields
 */

/**
 * @typedef type_exec_error_result
 * @property {number} step
 * @property {Error} error
 * @property {'connect'|'sql'|'kill'|'action'} point
 */

/**
 * @typedef type_exec_script_result
 * @property {string} [lock_on]
 * @property {string} [lock_off]
 * @property {string[]} queue
 * @property {string} [name_server]
 * @property {string} [name_database]
 * @property {string} [name_database_after_exec]
 * @property {number} [spid]
 * @property {string} [spguid]
 * @property {function} get_plain_query
 */

/**
 * @typedef type_exec_table_result
 * @property {Object} schema
 * @property {number} step
 * @property {number} table_index
 * @property {Object[]} rows
 */

/**
 * @callback function_tables_to_xml_file1
 * @param {Error} error
 * @param {string} xml
 */

/**
 * @callback function_tables_to_xml_file2
 * @param {string} [full_file_name] //if empty, see xml as a string in callback
 * @param {string[]} table_name_list
 * @param {function_tables_to_xml_file1} callback
 */

/**
 * @typedef type_performance
 * @property {number} perfomance_exec_before
 * @property {number} perfomance_exec_after
 * @property {number[]} duration_exec_millisecond
 */

/**
 * @typedef type_exec_result
 * @property {type_exec_error_result} handle_error
 * @property {type_exec_lock_result} [lock]
 * @property {type_exec_script_result} [script]
 * @property {type_exec_correction_utc_result} [correction_utc]
 * @property {type_performance} performance
 * @property {type_exec_table_result[]} [tables]
 * @property {function_tables_to_xml_file2} [tables_to_xml]
 */

/**
 * @typedef type_exec_runtime
 * @property {'spid'|'print'|'table'} type
 * @property {type_exec_spid} [spid]
 * @property {type_exec_print} [print]
 * @property {type_exec_table_result} [table]
 */

/**
 * @typedef type_exec_spid
 * @property {string} spguid
 * @property {number} spid
 * @property {boolean} exists
 * @property {boolean} killed
 */

/**
 * @typedef type_exec_print
 * @property {string} message
 * @property {string} procname
 * @property {boolean} is_error
 */

/**
 * @callback callback_exec_part
 * @param {Object} error
 */

/**
 * @callback callback_exec
 * @param {type_exec_result} result
 * @param {type_exec_runtime} runtime
 */

/**
 * @callback callback_exec_runtime
 * @param {type_exec_runtime} runtime
 */

/**
 * @callback callback_print
 * @param {type_exec_print} print
 */

/**
 * @callback callback_table
 * @param {type_exec_table_result} table
 */

/**
 * @callback callback_action
 * @param {type_exec_result} result
 * @param {Object[]} named_tables
 * @param {string[]} warning
 */

/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter
/** @private */
const lib_os = require('os')
/** @private */
const lib_sql = require('mssql')
/** @private */
const lib_vconv = require('viva-convert')
/** @private */
const lib_vaction = require('./actions.js')
/** @private */
const lib_vschedule = require('./schedules.js')
/** @private */
const lib_xml = require('./xml.js')
/** @private */
const lib_sql_util = require('./utils.js')
/** @private */
const { performance } = require('perf_hooks')

module.exports = Server_mssql
lib_util.inherits(Server_mssql, lib_event)
Server_mssql.prototype.emit = Server_mssql.prototype.emit || undefined
Server_mssql.prototype.on = Server_mssql.prototype.on || undefined

/**
* @class  (license MIT) library for work with Microsoft SQL Server, full example - see example.js
*/
function Server_mssql() {
    if (!(this instanceof Server_mssql)) return new Server_mssql()
    lib_event.call(this)
}

/** @private @type {type_connection_config} */
Server_mssql.prototype.connection_config = undefined
/** @type {boolean} */
Server_mssql.prototype.connected = false
/** @type {number} */
Server_mssql.prototype.timezone_offset = undefined
/** @private @type {lib_vaction} */
Server_mssql.prototype.action = undefined
/** @private @type {lib_vschedule} */
Server_mssql.prototype.schedule = undefined
/** @private @type {string[]} */
Server_mssql.prototype.guid_list = []
/** @private @type {NodeJS.Timer} */
Server_mssql.prototype.spid_list_clear = undefined
/** @private @type {type_exec_spid[]} */
Server_mssql.prototype.spid_list = []

/**
 * init library, check connect to MS SQL Server
 * @param {type_connection_config} connection_config connection setting
 * @param {callback_error} callback
 */
Server_mssql.prototype.init = function (connection_config, callback) {
    this.connected = false
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        if (lib_vconv.isAbsent(connection_config)) {
            throw new Error ('connection_config is empty')
        }
        if (lib_vconv.isEmpty(connection_config.server)) {
            throw new Error ('connection_config.server is empty')
        }
        if (lib_vconv.isAbsent(connection_config.password)) {
            throw new Error ('connection_config.password is empty')
        }
        if (lib_vconv.toInt(connection_config.again_connect_count, 0) > 0 && lib_vconv.toInt(connection_config.again_connect_timeout, 0) <= 0) {
            throw new Error ('connection_config.again_connect_count is not empty, but connection_config.again_connect_timeout is empty')
        }

        this.schedule = new lib_vschedule()
        this.schedule.server_mssql = this
        this.schedule.on('error', (job, exec_result) => {
            this.emit('schedule-error', job, exec_result)
        })
        this.schedule.on('result', (job, exec_result) => {
            this.emit('schedule-result', job, exec_result)
        })
        this.schedule.on('log', (job, log) => {
            this.emit('schedule-log', job, log)
        })

        this.connection_config = connection_config

        this.connect((error, connection) => {
            if (!lib_vconv.isAbsent(error)) {
                // @ts-ignore
                if (!lib_vconv.isAbsent(connection)) connection.pool_close()
                connection = undefined
                this.connected = false
                this.connection_config = undefined
                callback(error)
                return
            }

            let self = this
            if (lib_vconv.isAbsent(this.spid_list_clear)) {
                this.spid_list_clear = setInterval(function() {
                    self.spid_list = self.spid_list.filter(f => f.exists === true)
                }, 1000 * 60 * 10) //10 min
            }

            let query = 'SELECT DATEDIFF(MINUTE,GETDATE(), GETUTCDATE()) [timezone_offset];'
            if (lib_vconv.toBool(connection_config.action_allow, false)) {
                query = query.concat(' SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE, COLUMN_DEFAULT FROM INFORMATION_SCHEMA.[COLUMNS] WITH (NOLOCK) ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION, COLUMN_NAME')
            }
            connection.query(query, (error, result) => {
                if (!lib_vconv.isAbsent(error) && error !== null) {
                    // @ts-ignore
                    if (!lib_vconv.isAbsent(connection)) connection.pool_close()
                    connection = undefined
                    this.connected = false
                    this.connection_config = undefined
                    callback(error)
                    return
                }

                this.timezone_offset = lib_vconv.toInt(result.recordsets[0][0].timezone_offset)

                if (lib_vconv.toBool(connection_config.action_allow, false)) {
                    this.action = new lib_vaction()
                    this.action.server_mssql = this
                    result.recordsets[1].forEach(row => {
                        this.action.column_list.push({
                            database: row.TABLE_CATALOG,
                            schema: row.TABLE_SCHEMA,
                            table: row.TABLE_NAME,
                            column: row.COLUMN_NAME,
                            type: row.DATA_TYPE,
                            nullable: lib_vconv.toBool(row.IS_NULLABLE,true),
                            len: lib_vconv.toInt(row.CHARACTER_MAXIMUM_LENGTH),
                            precision: lib_vconv.toInt(row.NUMERIC_PRECISION),
                            scale: lib_vconv.toInt(row.NUMERIC_SCALE),
                            default: lib_vconv.toString(row.COLUMN_DEFAULT)
                        })
                    })
                }
                // @ts-ignore
                if (!lib_vconv.isAbsent(connection)) connection.pool_close()
                connection = undefined
                this.connected = true
                callback(undefined)
            })
        })
    } catch (error) {
        this.connected = false
        this.connection_config = undefined
        callback(error)
    }
}

/**
 * @callback callback_get_connection_pool
 * @param {Error} error
 * @param {lib_sql.ConnectionPool} connection_pool
 *//**
 * @private
 * @param {type_connection_config} connection_config connection setting
 * @param {callback_get_connection_pool} callback
 */
Server_mssql.prototype.get_connection_pool = function (connection_config, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        let connection_pool = undefined
        let internal_connection_config = this.internal_connection_config(connection_config)

        let again_connect_count = 0
        if (lib_vconv.toInt(connection_config.again_connect_count, 0) > 0 && lib_vconv.toInt(connection_config.again_connect_timeout, 0) > 0) {
            again_connect_count = lib_vconv.toInt(connection_config.again_connect_count, 0)
        }

        let timer = setTimeout(function tick() {
            try {
                connection_pool = new lib_sql.ConnectionPool(internal_connection_config, error => {
                    if (!lib_vconv.isEmpty(error)) {
                        again_connect_count--
                        if (again_connect_count > 0) {
                            timer = setTimeout(tick, connection_config.again_connect_timeout)
                        } else {
                            clearTimeout(timer)
                            callback(error, undefined)
                        }
                    } else {
                        clearTimeout(timer)
                        callback(undefined, connection_pool)
                    }
                })
            } catch (error) {
                clearTimeout(timer)
                callback(error, undefined)
            }
        }, 0)
    } catch (error) {
        callback(error, undefined)
    }
}

/**
 * @callback callback_connect
 * @param {Error} error
 * @param {lib_sql.Request} connection_pool
 *//**
 * connect to MS SQL Server
 * @private
 * @param {callback_connect} callback callback function, (error, connection) => {}
 */
Server_mssql.prototype.connect = function (callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    let connection
    try {
        this.get_connection_pool(this.connection_config, (error, pool) => {
            try {
                if (!lib_vconv.isAbsent(error)) {
                    callback(error, undefined)
                    return
                }
                pool.on('error', error => {
                    this.emit('error', error)
                })
                connection = new lib_sql.Request(pool)
                // @ts-ignore
                connection.pool_close = function () {
                    try {
                        // @ts-ignore
                        this.parent.removeAllListeners('error')
                        // @ts-ignore
                        this.parent.close()
                        // @ts-ignore
                        this.parent = undefined
                    } catch (e) {}
                }
                callback(undefined, connection)
            } catch (error) {
                connection = undefined
                callback(error, undefined)
            }
        })
    } catch (error) {
        connection = undefined
        callback(error, undefined)
    }
}

/**
 * exec query or queries
 * @param {string | string[]} queries query or array of queries (for run on one open connection)
 * @param {type_exec_option} [options]
 * @param {callback_exec} callback
 */
Server_mssql.prototype.exec = function (queries, options, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('viva-server-mssql.exec(...callback) - callback is not function')
    }
    /** @privare @type {type_exec_result} */
    let exec_result = {
        handle_error: {
            step: undefined,
            error: undefined,
            point: undefined
        },
        performance: {
            duration_exec_millisecond: [],
            perfomance_exec_after: undefined,
            perfomance_exec_before: undefined
        },
        lock: {
            allow: true
        },
        script: {
            queue: [],
            get_plain_query: (function() {
                let script = []

                let stop_index = exec_result.script.queue.length - 1
                if (exec_result.handle_error.point === 'sql' && !lib_vconv.isEmpty(exec_result.handle_error.step)) {
                    stop_index = lib_vconv.toInt(exec_result.handle_error.step, exec_result.script.queue.length - 1)
                }

                if (!lib_vconv.isAbsent(exec_result.performance) && !lib_vconv.isAbsent(exec_result.performance.duration_exec_millisecond) && exec_result.performance.duration_exec_millisecond.length > 0) {
                    script.push('--duration')
                    exec_result.performance.duration_exec_millisecond.forEach((item, index) => {
                        if (stop_index >= index) {
                            script.push(lib_vconv.format('--    script #{0}: {1}', [index.toString(), item.toString()]))
                        }
                    })
                    script.push('')
                }

                if (!lib_vconv.isEmpty(exec_result.script.name_database)) {
                    script.push('USE '.concat(exec_result.script.name_database))
                    script.push('GO')
                    script.push('')
                }

                if (!lib_vconv.isEmpty(exec_result.script.lock_on)) {
                    script.push('--lock on')
                    script.push('--'.concat(exec_result.script.lock_on))
                    if (exec_result.lock.allow !== true) {
                        script.push('--CHECK LOCK, EXECUTION STOP')
                        return script.join(lib_os.EOL).trim()
                    }
                    script.push('')
                }

                if (!lib_vconv.isAbsent(exec_result.script.queue) && exec_result.script.queue.length > 0) {
                    exec_result.script.queue.forEach((item, index) => {
                        if (stop_index >= index) {
                            script.push(lib_vconv.format('--script #{0}', index.toString()))
                            script.push(item)
                            if (stop_index === index && !lib_vconv.isEmpty(exec_result.handle_error.step) && !lib_vconv.isAbsent(exec_result.handle_error.error) && exec_result.handle_error.point === 'sql') {
                                script.push('/*')
                                script.push(exec_result.handle_error.error.toString())
                                script.push('*/')
                            } else {
                                script.push('GO')
                                script.push('')
                            }
                        }
                    })
                }

                if (!lib_vconv.isEmpty(exec_result.script.lock_off)) {
                    script.push('--lock off')
                    script.push('--'.concat(exec_result.script.lock_off))
                    script.push('')
                }

                return script.join(lib_os.EOL).trim()
            })
        },
        correction_utc: {
            mode: undefined,
            offset: undefined
        },
        tables: [],
        tables_to_xml: (function (full_file_name, table_name_list, callback) {
            lib_xml.write(full_file_name, exec_result.tables, table_name_list, (error, stream) => {
                if (lib_vconv.isFunction(callback)) {
                    callback(error, stream)
                }
            })
        })
    }

    try {
        if (this.connected !== true) {
            throw new Error ('no connection to sql server, maybe not exec INIT')
        }

        if (lib_vconv.isAbsent(queries)) {
            throw new Error ('queries is empty')
        }
        if (Array.isArray(queries)) {
            exec_result.script.queue = queries
        } else {
            exec_result.script.queue.push(lib_vconv.toString(queries, ''))
        }
        if (exec_result.script.queue.length <= 0) {
            throw new Error ('queries is empty')
        }

        if (lib_vconv.isAbsent(options)) options = {}
        if (lib_vconv.isAbsent(options.lock)) options.lock = {}
        if (lib_vconv.isAbsent(options.correction_utc)) options.correction_utc = {}
        options.noname_row_beautify = lib_vconv.toBool(options.noname_row_beautify, false)
        options.chunk_rows = lib_vconv.toInt(options.chunk_rows, 0)
        options.chunk_msec = lib_vconv.toInt(options.chunk_msec, 0)
        options.check_spid = lib_vconv.toBool(options.check_spid, false)
        options.check_print = lib_vconv.toBool(options.check_print, false)
        options.check_database_after_exec = lib_vconv.toBool(options.check_database_after_exec, false)
        options.type_ext = lib_vconv.toBool(options.type_ext, false)

        exec_result.lock.key = options.lock.key
        exec_result.lock.wait = options.lock.wait
        exec_result.lock.message = options.lock.message
        exec_result.correction_utc = {
            mode: lib_vconv.toString(options.correction_utc.mode),
            offset: lib_vconv.toInt(options.correction_utc.offset)
        }
        exec_result.performance.perfomance_exec_before = performance.now()

        this.connect((error, connection) => {
            if (!lib_vconv.isAbsent(error)) {
                exec_result.handle_error.error = error
                exec_result.handle_error.point = 'connect'
                callback(exec_result, undefined)
                return
            }
            connection.stream = true
            exec_result.script.name_database = this.connection_config.database
            exec_result.script.name_server = this.connection_config.server

            exec_lock_on(connection, exec_result, () => {
                if (exec_result.lock.allow !== true) {
                    // @ts-ignore
                    connection.pool_close()
                    connection = undefined
                    callback(exec_result, undefined)
                    return
                }

                exec_check_spid(connection, exec_result, options, () => {
                    /** @type {type_exec_spid} */
                    let spid_item = undefined
                    if (!lib_vconv.isEmpty(exec_result.script.spid) && !lib_vconv.isEmpty(exec_result.script.spguid)) {
                        this.spid_list.filter(f => f.spid === exec_result.script.spid && f.exists === true).forEach(item => {
                            item.exists = false
                        })
                        spid_item = {spguid: exec_result.script.spguid, spid: exec_result.script.spid, exists: true, killed: false}
                        this.spid_list.push(spid_item)

                        callback(undefined, {type: 'spid', spid: {spguid: exec_result.script.spguid, spid: exec_result.script.spid, exists: undefined, killed: undefined}})
                    }
                    exec_all(connection, exec_result, options, spid_item, callback_runtime => {
                        if (!lib_vconv.isAbsent(callback_runtime)) {
                            if (options.noname_row_beautify === true && callback_runtime.type === 'table') {
                                noname_row_beautify(callback_runtime.table, options.type_ext)
                            }
                            callback(undefined, callback_runtime)
                            return
                        }
                        exec_lock_off(connection, exec_result, () => {
                            exec_check_database_after_exec(connection, exec_result, options, () => {
                                if (!lib_vconv.isAbsent(spid_item)) {
                                    spid_item.exists = false
                                }
                                // @ts-ignore
                                connection.pool_close()
                                connection = undefined

                                if (options.noname_row_beautify === true && !lib_vconv.isAbsent(exec_result.tables)) {
                                    exec_result.tables.forEach(table => noname_row_beautify(table, options.type_ext))
                                }

                                exec_result.performance.perfomance_exec_after = performance.now()
                                callback(exec_result, undefined)
                                // exec_postprocessor(exec_result, options, () => {
                                //     callback(exec_result, undefined)
                                // })
                            })
                        })
                    })
                })
            })
        })
    } catch (error) {
        exec_result.handle_error.error = error
        callback(exec_result, undefined)
    }
}

Server_mssql.prototype.kill = function(spguid, callback) {
    if (!this.spid_list.some(f => f.spguid === spguid && f.exists === true)) {
        if (lib_vconv.isFunction(callback)) callback()
        return
    }

    this.connect((error, connection) => {
        if (!lib_vconv.isAbsent(error)) {
            // @ts-ignore
            if (!lib_vconv.isAbsent(connection)) connection.pool_close()
            connection = undefined
            if (lib_vconv.isFunction(callback)) callback()
            return
        }

        let spid_item = this.spid_list.find(f => f.spguid === spguid && f.exists === true)
        if (lib_vconv.isEmpty(spid_item)) {
            if (lib_vconv.isFunction(callback)) callback()
            return
        }
        spid_item.killed = true
        connection.query("KILL ".concat(spid_item.spid.toString()), (error, result) => {
            spid_item.exists = false
            if (lib_vconv.isFunction(callback)) callback()
            return
        })
    })
}

/**
 * @private
 * connection config in tedious format
 * @param {type_connection_config} [connection_config]
 * @returns {type_internal_connection_config}
 */
Server_mssql.prototype.internal_connection_config = function (connection_config) {
    if (lib_vconv.isAbsent(connection_config)) {
        connection_config = this.connection_config
    }

    /** @type type_internal_connection_config */
    let config = {
        server: connection_config.server,
        database: lib_vconv.border_del(lib_vconv.toString(connection_config.database, 'tempdb'),'[',']'),
        user: undefined,
        password: connection_config.password,
        domain: undefined,
        connectionTimeout: lib_vconv.toInt(connection_config.connection_timeout, 15000),
        requestTimeout: lib_vconv.toInt(connection_config.execution_timeout, 0),
        pool: {
            max: 200,
            min: 0,
            idleTimeoutMillis: 30000
        },
        options: {
            useUTC: lib_vconv.toBool(connection_config.useUTC, true),
            encrypt: lib_vconv.toBool(connection_config.encrypt_connection, false),
            enableArithAbort: false,
            appName: lib_vconv.toString(connection_config.app_name, 'viva-server-mssql'),
            trustedConnection: true
        }
    }

    if (!lib_vconv.isEmpty(connection_config.login) && !lib_vconv.isEmpty(connection_config.password)) {
        config.user = connection_config.login
    } else {
        config.user = lib_os.userInfo().username
        config.domain = lib_os.hostname().toUpperCase()
    }

    return config
}

/**
 * add one action
 * @param {type_action} action single action
 * @return {string} error that prevented load action
 */
Server_mssql.prototype.action_add = function (action) {
    try {
        if (lib_vconv.isAbsent(this.action)) {
            throw new Error ('Action subsystem disabled')
        }
        return this.action.add(action)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-server-mssql.action_add(...)')
    }
}

/**
 * delete action from list
 * @param {string} key action key
 */
Server_mssql.prototype.action_del = function (key) {
    try {
        return this.action.del(key)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-server-mssql.action_del(...)')
    }
}

/**
 * @typedef type_action_find
 * @property {string} key
 * @property {string} title
 * @property {string[]} sql_result_list
 * @property {string} note
 * @property {string[]} error_list
 */
/**
 * find action key and load errors
 * @param {string} key action key
 * @returns {type_action_find}
 */
Server_mssql.prototype.action_check = function (key) {
    try {
        if (lib_vconv.isEmpty(key)) return undefined
        let action = this.action.list.find(f => f.key.toLowerCase() === key.toLowerCase())
        if (lib_vconv.isAbsent(action)) return undefined

        return {
            key: key,
            title: (lib_vconv.isAbsent(action.action) ? undefined : action.action.title),
            sql_result_list: (lib_vconv.isAbsent(action.sql_result_list) ? [] : action.sql_result_list.map(m => { return m.name })),
            note: (lib_vconv.isAbsent(action.action) ? undefined : action.action.note),
            error_list: (lib_vconv.isAbsent(action.error_list) ? [] : action.error_list)
        }
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-server-mssql.action_find(...)')
    }
}

/**
 * exec action
 * @param {string} key uniq key of action
 * @param {any} params
 * @param {callback_action} callback
 */
Server_mssql.prototype.action_exec = function (key, params, callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    if (lib_vconv.isAbsent(this.action)) {
        throw new Error ('Action subsystem disabled')
    }
    if (lib_vconv.isEmpty(key)) {
        throw new Error ('Action key is empty')
    }

    this.action.exec(key, params, callback)
}

/**
 * add new schedule or replace existsing schedule
 * @param {lib_vschedule.type_job} job
 */
Server_mssql.prototype.schedule_go = function (job) {
    try {
        this.schedule.go(job)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-server-mssql.schedule_go(...)')
    }
}

/**
 * remove existsing schedule
 * @param {string} job_key
 */
Server_mssql.prototype.schedule_delete = function (job_key) {
    try {
        this.schedule.delete(job_key)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-server-mssql.schedule_delete(...)')
    }
}

/**
 * @callback callback_get_newid
 * @param {Error} error
 * @param {string} guid
 *//**
 * get new guid
 * @param {callback_get_newid} callback error, guid
 */
Server_mssql.prototype.get_newid = function (callback) {
    if (!lib_vconv.isFunction(callback)) {
        throw new Error('callback is not function')
    }
    try {
        let g = this.guid_list.shift()
        if (!lib_vconv.isEmpty(g)) {
            callback(undefined, g)
            return
        }
        let script = "".concat(
            "DECLARE @i INT = 0", lib_os.EOL,
            "DECLARE @g TABLE (g UNIQUEIDENTIFIER)", lib_os.EOL,
            "WHILE @i < 100 BEGIN", lib_os.EOL,
            "   INSERT INTO @g(g) VALUES (NEWID())", lib_os.EOL,
            "   SET @i = @i + 1", lib_os.EOL,
            "END", lib_os.EOL,
            "SELECT g FROM @g"
        )

        this.exec(script, undefined, exec_result => {
            if (!lib_vconv.isAbsent(exec_result.handle_error.error)) {
                callback(exec_result.handle_error.error, undefined)
                return
            }
            let list = []
            exec_result.tables[0].rows.forEach(r => {
                list.push(r.g)
            })
            this.guid_list = list
            this.get_newid(callback)
        })

    } catch (error) {
        callback(error, undefined)
    }
}

/**
 * get date in ms sql server
 * @param {Date} [d] local date regarding which to get the date on the server, default - now
 * @returns {Date}
 */
Server_mssql.prototype.date_in_sql = function (d) {
    let local_date = lib_vconv.toDate(d, new Date())
    let offset = lib_vconv.toInt(this.timezone_offset, 0)

    let minute_offset = offset - local_date.getTimezoneOffset()
    return lib_vconv.dateAdd('minute', minute_offset, local_date)
}

/**
 * @private
 * @param {any} connection
 */
function connection_emit_unsubscribe(connection) {
    if (lib_vconv.isAbsent(connection)) return
    connection.removeAllListeners('error')
    connection.removeAllListeners('recordset')
    connection.removeAllListeners('row')
    connection.removeAllListeners('done')
}

/**
 * @private
 * @param {Object} connection
 * @param {type_exec_result} exec_result
 * @param {callback_empty} callback
 */
function exec_lock_on(connection, exec_result, callback) {
    if (lib_vconv.isEmpty(exec_result.lock.key)) {
        callback()
        return
    }
    let wait = lib_vconv.toInt(exec_result.lock.wait, 0)
    if (wait < 0) wait = 0

    let lock_result = undefined

    exec_result.script.lock_on = "".concat(
        "DECLARE @result INT",
        ";EXEC @result=sp_getapplock",
        " @Resource='",exec_result.lock.key,"'",
        ",@LockOwner='Session'",
        ",@LockMode='exclusive'",
        ",@LockTimeOut=", wait.toString(),
        ';SELECT @result [result]'
    )

    connection.on('error', error => {
        exec_result.lock.allow = false
        exec_result.handle_error.error = error
        exec_result.handle_error.point = 'sql'
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.on('row', row => {
        lock_result = lib_vconv.toInt(row.result, -9999)
    })
    connection.on('done', () => {
        if (lock_result < 0) {
            exec_result.lock.allow = false
        }
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.batch(exec_result.script.lock_on)
}

/**
 * @private
 * @param {Object} connection
 * @param {type_exec_result} exec_result
 * @param {callback_empty} callback
 */
function exec_lock_off(connection, exec_result, callback) {
    if (lib_vconv.isEmpty(exec_result.lock.key)) {
        callback()
        return
    }

    if (!lib_vconv.isAbsent(exec_result.handle_error) && exec_result.handle_error.point === 'kill') {
        callback()
        return
    }

    exec_result.script.lock_off = "".concat(
        "EXEC sp_releaseapplock",
        " @Resource='",exec_result.lock.key,"'",
        ",@LockOwner='Session'",
    )

    connection.on('error', error => {
        // exec_result.handle_error.error = error
        // exec_result.handle_error.point = 'sql'
        // connection_emit_unsubscribe(connection)
        // callback()
    })
    connection.on('done', () => {
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.batch(exec_result.script.lock_off)
}

/**
 * @private
 * @param {Object} connection
 * @param {type_exec_result} exec_result
 * @param {type_exec_option} options
 * @param {callback_empty} callback
 */
function exec_check_database_after_exec(connection, exec_result, options, callback) {
    if (options.check_database_after_exec !== true) {
        callback()
        return
    }

    if (!lib_vconv.isAbsent(exec_result.handle_error) && exec_result.handle_error.point === 'kill') {
        callback()
        return
    }

    connection.on('error', error => {
        // exec_result.handle_error.error = error
        // exec_result.handle_error.point = 'sql'
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.on('row', row => {
        exec_result.script.name_database_after_exec = lib_vconv.toString (row.result)
    })
    connection.on('done', () => {
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.batch("SELECT DB_NAME() result")
}

/**
 * @private
 * @param {Object} connection
 * @param {type_exec_result} exec_result
 * @param {type_exec_option} options
 * @param {callback_empty} callback
 */
function exec_check_spid(connection, exec_result, options, callback) {
    if (options.check_spid !== true) {
        callback()
        return
    }
    connection.on('error', error => {
        exec_result.handle_error.error = error
        exec_result.handle_error.point = 'sql'
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.on('row', row => {
        exec_result.script.spid = lib_vconv.toInt (row.spid)
        exec_result.script.spguid = lib_vconv.toString (row.spguid)
    })
    connection.on('done', () => {
        connection_emit_unsubscribe(connection)
        callback()
    })
    connection.batch("SELECT @@SPID spid, NEWID() spguid")
}

/**
 * @private
 * @param {any} connection
 * @param {type_exec_result} exec_result
 * @param {type_exec_option} option
 * @param {type_exec_spid} spid_item
 * @param {callback_exec_runtime} callback
 */
function exec_all(connection, exec_result, option, spid_item, callback) {
    let current_step_index = 0
    let current_table_index = -1
    /** @private @type {number} */
    let current_time_start
    /** @private @type {number[]} */
    let duration_millisecond = []
    /** @private @type {type_exec_table_result[]} */
    let current_tables = []
    /** @private @type {number} */
    let last_table_index_send_rows = -1

    let performance_chunk = (option.chunk_msec > 0 ? performance.now() : undefined)

    if (option.check_print === true) {
        connection.on('info', info => {
            callback({type: 'print', print: {message: info.message, procname: info.procName, is_error: false}})
        })
    }

    connection.on('error', error => {
        if (!lib_vconv.isAbsent(error.originalError) && !lib_vconv.isAbsent(error.originalError.info) && error.originalError.info.event === 'errorMessage' && error.originalError.info.number === 50000) {
            if (option.check_print === true) {
                callback({type: 'print', print: {message: error.originalError.message, procname: error.originalError.procName, is_error: true}})
            }
            return
        }

        exec_result.handle_error.step = current_step_index
        exec_result.handle_error.point = 'sql'
        if (!lib_vconv.isAbsent(error)) {
            exec_result.handle_error.error = error
        } else {
            exec_result.handle_error.error = new Error('UNKNOWN ERROR')
        }

        if (!lib_vconv.isAbsent(spid_item) && spid_item.killed === true) { // !lib_vconv.isAbsent(error.originalError) && !lib_vconv.isAbsent(error.originalError.info) && error.originalError.info.number === 596) {
            if (lib_vconv.toString(error.code,'').toUpperCase() === 'UNKNOWN') {
                exec_result.handle_error.error = new Error('process is killed')
            }
            exec_result.handle_error.point = 'kill'
        }
    })

    connection.on('done', () => {
        if (current_step_index >= 0) {
            duration_millisecond.push(performance.now() - current_time_start)
        }
        if (current_step_index + 1 < exec_result.script.queue.length && lib_vconv.isAbsent(exec_result.handle_error.error)) {
            current_step_index++
            current_time_start = performance.now()
            connection.batch(exec_result.script.queue[current_step_index])
        } else {
            connection_emit_unsubscribe(connection)
            exec_result.performance.duration_exec_millisecond = duration_millisecond

            if (option.chunk_rows > 0 || option.chunk_msec > 0) {
                if (current_tables.length > 0 && (current_tables[current_tables.length-1].rows.length > 0 || last_table_index_send_rows !== current_tables.length-1)) {
                    last_table_index_send_rows = current_tables.length-1
                    callback({type: 'table', table: clone_table(current_tables[last_table_index_send_rows])})
                }
            } else {
                exec_result.tables = current_tables
            }

            callback(undefined)
        }
    })

    connection.on('recordset', columns => {
        if (option.chunk_rows > 0 || option.chunk_msec > 0) {
            if (current_tables.length > 0 && (current_tables[current_tables.length-1].rows.length > 0 || last_table_index_send_rows !== current_tables.length-1)) {
                last_table_index_send_rows = current_tables.length-1
                callback({type: 'table', table: clone_table(current_tables[last_table_index_send_rows])})
                current_tables[current_table_index].rows = []
            }
        }

        if (option.type_ext === true) {
            let all_types = lib_sql_util.types_sql()

            for (let column_name in columns) {
                let column = columns[column_name]
                if (lib_vconv.isAbsent(column.type)) continue

                let declaration = lib_vconv.toString(column.type.declaration,'').toLowerCase()

                column.type_name = declaration
                let charlen = column.length
                if (!lib_vconv.isAbsent(charlen)) {
                    let type = all_types.find(f => f.type === column.type_name)
                    if (!lib_vconv.isAbsent(type)) {
                        if (type.len === 'allow' && charlen >= 65535) {
                            charlen = -1
                            column.type_charlen = charlen
                        } else if (!lib_vconv.isAbsent(type.bytes_on_char) && type.bytes_on_char !== 0) {
                            charlen = Math.floor(charlen / type.bytes_on_char)
                            column.type_charlen = charlen
                        }
                    }
                }
                column.type_title = lib_sql_util.script_declare_by_type_sql(column.type_name, column.precision, column.scale, charlen)
            }
        }

        current_table_index++
        current_tables.push({
            schema: columns,
            step: current_step_index,
            rows: [],
            table_index: current_table_index
        })

    })

    connection.on('row', row => {
        current_tables[current_table_index].rows.push(row)

        let chunk_already_sended = false

        if (option.chunk_rows > 0 && current_tables[current_table_index].rows.length >= option.chunk_rows) {
            last_table_index_send_rows = current_table_index
            callback({type: 'table', table: clone_table(current_tables[last_table_index_send_rows])})
            current_tables[current_table_index].rows = []
            chunk_already_sended = true
        }
        if (option.chunk_msec > 0) {
            let p_now = performance.now()
            if (chunk_already_sended === true) {
                performance_chunk = p_now
            } else if (p_now - performance_chunk > option.chunk_msec) {
                performance_chunk = p_now
                last_table_index_send_rows = current_table_index
                callback({type: 'table', table: clone_table(current_tables[last_table_index_send_rows])})
                current_tables[current_table_index].rows = []
            }
        }
    })

    current_time_start = performance.now()
    connection.batch(exec_result.script.queue[0])
}

/**
 * @param {type_exec_table_result} from
 * @return {type_exec_table_result}
 */
function clone_table(from) {
    if (lib_vconv.isAbsent(from)) return undefined
    return {
        step: from.step,
        table_index: from.table_index,
        rows: (lib_vconv.isAbsent(from.rows) ? undefined : from.rows.map(m => m)),
        schema: Object.assign({}, from.schema)
    }
}

/**
 * @param {type_exec_table_result} table
 * @param {boolean} type_ext
 */
function noname_row_beautify(table, type_ext) {
    try {
        if (lib_vconv.isAbsent(table)) return
        if (lib_vconv.isAbsent(table.schema[''])) return
        if (table.rows.length <= 0) {
            delete table.schema['']
            return
        }
        let noname_count = (Array.isArray(table.rows[0]['']) ? table.rows[0][''].length : 1)

        if (noname_count === 1) {

            let new_schema = {}
            Object.keys(table.schema).forEach(key => {
                if (key === '') {
                    new_schema['noname#0'] = { ...table.schema[''], noname_row_beautify: true, name: 'noname#0'}
                    // let newPair = { ['noname#0']: {noname_row_beautify: true,  ...table.schema['']} }
                    // new_schema = { ...new_schema, ...newPair }
                } else {
                    new_schema[key] = table.schema[key]
                    // new_schema = { ...new_schema, [key]: table.schema[''] }
                }
            })
            //new_schema['noname#0'].noname_row_beautify = true
            table.schema = new_schema

            table.rows.forEach(row => {
                row['noname#0'] = row['']
                delete row['']
            })
            return
        }

        let index_exists = []
        for (let prop in table.schema) {
            if (!lib_vconv.isEmpty(prop)) {
                let index = table.schema[prop].index
                if (!lib_vconv.isEmpty(index)) {
                    index_exists.push({name: prop, index: index, added_in_new_schema: false})
                }
            }
        }
        index_exists.sort((a,b) => a.index - b.index)

        let noname_current = 0
        let new_schema = {}

        let column_index = 0
        while(noname_current < noname_count || index_exists.some(f => f.added_in_new_schema === false)) {
            let find_exists = index_exists.find(f => f.index === column_index)
            if (lib_vconv.isAbsent(find_exists)) {
                if (noname_current < noname_count) {
                    let noname_name = 'noname#'.concat(noname_current.toString())
                    new_schema[noname_name] = {
                        index: column_index,
                        name: noname_name,
                        length: 65535,
                        caseSensitive: false,
                        identity: false,
                        nullable: true,
                        precision: undefined,
                        readOnly: true,
                        scale: undefined,
                        type: {
                            declaration: "nvarchar",
                            length: 1,
                            name:"NVarChar"
                        },
                        noname_row_beautify: true,
                    }

                    if (type_ext === true) {
                        new_schema[noname_name].type_charlen = -1
                        new_schema[noname_name].type_name = "nvarchar"
                        new_schema[noname_name].type_title = "NVARCHAR(MAX)"
                    }
                    noname_current++
                }
            } else {
                new_schema[find_exists.name] = table.schema[find_exists.name]
                find_exists.added_in_new_schema = true
            }
            column_index++
        }

        // for (let i = 0; i < index_exists[index_exists.length - 1].index + noname_count - 1; i++) {
        //     let find_exists = index_exists.find(f => f.index === i)
        //     if (lib_vconv.isAbsent(find_exists)) {
        //         if (noname_current < noname_count) {
        //             let noname_name = 'noname#'.concat(noname_current.toString())
        //             new_schema[noname_name] = {
        //                 index: i,
        //                 name: noname_name,
        //                 length: 65535,
        //                 caseSensitive: false,
        //                 identity: false,
        //                 nullable: true,
        //                 precision: undefined,
        //                 readOnly: true,
        //                 scale: undefined,
        //                 type: {
        //                     declaration: "nvarchar",
        //                     length: 1,
        //                     name:"NVarChar"
        //                 },
        //                 noname_row_beautify: true,
        //             }

        //             if (type_ext === true) {
        //                 new_schema[noname_name].type_charlen = -1
        //                 new_schema[noname_name].type_name = "nvarchar"
        //                 new_schema[noname_name].type_title = "NVARCHAR(MAX)"
        //             }

        //             noname_current++
        //         }
        //     } else {
        //         new_schema[find_exists.name] = table.schema[find_exists.name]
        //         find_exists.added_in_new_schema = true
        //     }
        // }
        // index_exists.filter(f => f.added_in_new_schema === false).forEach(item => {
        //     new_schema[item.name] = table.schema[item.name]
        // })
        table.schema = new_schema

        table.rows.forEach(row => {
            let row_with_noname = row['']
            if (Array.isArray(row_with_noname)) {
                row_with_noname.forEach((noname, noname_index) => {
                    row['noname#'.concat(noname_index.toString())] = (noname === null ? null : lib_vconv.toString(noname))
                })
            }
            delete row['']
        })

    } catch (error) {
        console.error(error)
    }
}

/**
 * @private
 * @param {any} error
 * @param {string} server
 * @param {string} [database]
 * @returns {string}
 */
function get_exec_error_text (error, server, database) {
    if (lib_vconv.isAbsent(error) || error === null) {
        return undefined
    }

    let targen_name = 'UNKNOWN'
    if (!lib_vconv.isEmpty(server)) {
        targen_name = lib_vconv.toString(server,'')
        if (!lib_vconv.isEmpty(database)) {
            targen_name = targen_name.concat(' (database ', lib_vconv.toString(database,''), ')')
        }
    }

    if (!lib_vconv.isEmpty(error.code)) {
        switch (error.code) {
            case 'ELOGIN':
                return 'error connect to MS SQL Server '.concat(targen_name,' - login failed(ELOGIN)')
            case 'ETIMEOUT':
                return 'error connect to MS SQL Server '.concat(targen_name,' - connection timeout(ETIMEOUT)')
            case 'EALREADYCONNECTED':
                return 'error connect to MS SQL Server '.concat(targen_name,' - database is already connected(EALREADYCONNECTED)')
            case 'EALREADYCONNECTING':
                return 'error connect to MS SQL Server '.concat(targen_name,' - already connecting to database(EALREADYCONNECTING)')
            case 'EINSTLOOKUP':
                return 'error connect to MS SQL Server '.concat(targen_name,' - instance lookup failed(EINSTLOOKUP)')
            case 'ESOCKET':
                return 'error connect to MS SQL Server '.concat(targen_name,' - socket error(ESOCKET)')
            case 'EREQUEST':
                if (!lib_vconv.isEmpty(error.message)) {
                    return 'error exec in MS SQL Server '.concat(targen_name,' - ',lib_vconv.toString(error.message,'UNKNOWN ERROR'))
                }
                break
            case 'ECANCEL':
                return 'error exec in MS SQL Server '.concat(targen_name,' - cancelled(ECANCEL)')
            case 'ENOCONN':
                return 'error exec in MS SQL Server '.concat(targen_name,' - no connection is specified for that request(ENOCONN)')
            case 'ENOTOPEN':
                return 'error exec in MS SQL Server '.concat(targen_name,' - connection not yet open(ENOTOPEN)')
            case 'ECONNCLOSED':
                return 'error exec in MS SQL Server '.concat(targen_name,' - connection is closed(ECONNCLOSED)')
            case 'ENOTBEGUN':
                return 'error exec in MS SQL Server '.concat(targen_name,' - transaction has not begun(ENOTBEGUN)')
            case 'EABORT':
                return 'error exec in MS SQL Server '.concat(targen_name,' - transaction was aborted (by user or because of an error)(EABORT)')
            default:
                break
        }
    }

    return lib_vconv.toErrorMessage(error, 'error work with MS SQL Server '.concat(targen_name))
}

/**
 * @param {number[]} nums
 * @returns {number[]}
 */
function find_disappeared_numbers (nums) {
    let missing = [];
    let seen = new Array(nums.length).fill(false);

    for (let i = 0; i < nums.length; i++) {
        seen[nums[i]-1] = true
    }

    for (let i = 0; i < seen.length; i++) {
        if (!seen[i]){
            missing.push(i+1)
        }
    }

    return missing
}