## Classes

<dl>
<dt><a href="#Server_mssql">Server_mssql</a></dt>
<dd><p>(license MIT) library for work with Microsoft SQL Server, full example - see example.js</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#clone_table">clone_table(from)</a> ⇒ <code><a href="#type_exec_table_result">type_exec_table_result</a></code></dt>
<dd></dd>
<dt><a href="#noname_row_beautify">noname_row_beautify(table, type_ext)</a></dt>
<dd></dd>
<dt><a href="#find_disappeared_numbers">find_disappeared_numbers(nums)</a> ⇒ <code>Array.&lt;number&gt;</code></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_connection_config">type_connection_config</a></dt>
<dd></dd>
<dt><a href="#type_action">type_action</a></dt>
<dd></dd>
<dt><a href="#type_generate_class_with_exec_actions">type_generate_class_with_exec_actions</a></dt>
<dd></dd>
<dt><a href="#callback_empty">callback_empty</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_error">callback_error</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#type_exec_option">type_exec_option</a></dt>
<dd></dd>
<dt><a href="#type_exec_lock_option">type_exec_lock_option</a></dt>
<dd></dd>
<dt><a href="#type_exec_lock_result">type_exec_lock_result</a></dt>
<dd></dd>
<dt><a href="#type_exec_correction_utc_option">type_exec_correction_utc_option</a></dt>
<dd></dd>
<dt><a href="#type_exec_correction_utc_result">type_exec_correction_utc_result</a></dt>
<dd></dd>
<dt><a href="#type_exec_error_result">type_exec_error_result</a></dt>
<dd></dd>
<dt><a href="#type_exec_script_result">type_exec_script_result</a></dt>
<dd></dd>
<dt><a href="#type_exec_table_result">type_exec_table_result</a></dt>
<dd></dd>
<dt><a href="#function_tables_to_xml_file1">function_tables_to_xml_file1</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#function_tables_to_xml_file2">function_tables_to_xml_file2</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#type_performance">type_performance</a></dt>
<dd></dd>
<dt><a href="#type_exec_result">type_exec_result</a></dt>
<dd></dd>
<dt><a href="#type_exec_runtime">type_exec_runtime</a></dt>
<dd></dd>
<dt><a href="#type_exec_spid">type_exec_spid</a></dt>
<dd></dd>
<dt><a href="#type_exec_print">type_exec_print</a></dt>
<dd></dd>
<dt><a href="#callback_exec_part">callback_exec_part</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_exec">callback_exec</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_exec_runtime">callback_exec_runtime</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_print">callback_print</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_table">callback_table</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_action">callback_action</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_get_connection_pool">callback_get_connection_pool</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#callback_connect">callback_connect</a> : <code>function</code></dt>
<dd></dd>
<dt><a href="#type_action_find">type_action_find</a></dt>
<dd></dd>
<dt><a href="#callback_get_newid">callback_get_newid</a> : <code>function</code></dt>
<dd></dd>
</dl>

<a name="Server_mssql"></a>

## Server\_mssql
(license MIT) library for work with Microsoft SQL Server, full example - see example.js

**Kind**: global class  

* [Server_mssql](#Server_mssql)
    * [.connected](#Server_mssql+connected) : <code>boolean</code>
    * [.timezone_offset](#Server_mssql+timezone_offset) : <code>number</code>
    * [.init(connection_config, callback)](#Server_mssql+init)
    * [.exec(queries, [options], callback)](#Server_mssql+exec)
        * [~exec_result](#Server_mssql+exec..exec_result)
    * [.action_add(action)](#Server_mssql+action_add) ⇒ <code>string</code>
    * [.action_del(key)](#Server_mssql+action_del)
    * [.action_check(key)](#Server_mssql+action_check) ⇒ [<code>type\_action\_find</code>](#type_action_find)
    * [.action_exec(key, params, callback)](#Server_mssql+action_exec)
    * [.schedule_go(job)](#Server_mssql+schedule_go)
    * [.schedule_delete(job_key)](#Server_mssql+schedule_delete)
    * [.get_newid(callback)](#Server_mssql+get_newid)
    * [.date_in_sql([d])](#Server_mssql+date_in_sql) ⇒ <code>Date</code>

<a name="Server_mssql+connected"></a>

### server_mssql.connected : <code>boolean</code>
**Kind**: instance property of [<code>Server\_mssql</code>](#Server_mssql)  
<a name="Server_mssql+timezone_offset"></a>

### server_mssql.timezone\_offset : <code>number</code>
**Kind**: instance property of [<code>Server\_mssql</code>](#Server_mssql)  
<a name="Server_mssql+init"></a>

### server_mssql.init(connection_config, callback)
init library, check connect to MS SQL Server

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| connection_config | [<code>type\_connection\_config</code>](#type_connection_config) | connection setting |
| callback | [<code>callback\_error</code>](#callback_error) |  |

<a name="Server_mssql+exec"></a>

### server_mssql.exec(queries, [options], callback)
exec query or queries

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| queries | <code>string</code> \| <code>Array.&lt;string&gt;</code> | query or array of queries (for run on one open connection) |
| [options] | [<code>type\_exec\_option</code>](#type_exec_option) |  |
| callback | [<code>callback\_exec</code>](#callback_exec) |  |

<a name="Server_mssql+exec..exec_result"></a>

#### exec~exec\_result
**Kind**: inner property of [<code>exec</code>](#Server_mssql+exec)  
**Privare**: @type {type_exec_result}  
<a name="Server_mssql+action_add"></a>

### server_mssql.action\_add(action) ⇒ <code>string</code>
add one action

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  
**Returns**: <code>string</code> - error that prevented load action  

| Param | Type | Description |
| --- | --- | --- |
| action | [<code>type\_action</code>](#type_action) | single action |

<a name="Server_mssql+action_del"></a>

### server_mssql.action\_del(key)
delete action from list

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | action key |

<a name="Server_mssql+action_check"></a>

### server_mssql.action\_check(key) ⇒ [<code>type\_action\_find</code>](#type_action_find)
find action key and load errors

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | action key |

<a name="Server_mssql+action_exec"></a>

### server_mssql.action\_exec(key, params, callback)
exec action

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | uniq key of action |
| params | <code>any</code> |  |
| callback | [<code>callback\_action</code>](#callback_action) |  |

<a name="Server_mssql+schedule_go"></a>

### server_mssql.schedule\_go(job)
add new schedule or replace existsing schedule

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type |
| --- | --- |
| job | <code>lib\_vschedule.type\_job</code> | 

<a name="Server_mssql+schedule_delete"></a>

### server_mssql.schedule\_delete(job_key)
remove existsing schedule

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type |
| --- | --- |
| job_key | <code>string</code> | 

<a name="Server_mssql+get_newid"></a>

### server_mssql.get\_newid(callback)
get new guid

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| callback | [<code>callback\_get\_newid</code>](#callback_get_newid) | error, guid |

<a name="Server_mssql+date_in_sql"></a>

### server_mssql.date\_in\_sql([d]) ⇒ <code>Date</code>
get date in ms sql server

**Kind**: instance method of [<code>Server\_mssql</code>](#Server_mssql)  

| Param | Type | Description |
| --- | --- | --- |
| [d] | <code>Date</code> | local date regarding which to get the date on the server, default - now |

<a name="clone_table"></a>

## clone\_table(from) ⇒ [<code>type\_exec\_table\_result</code>](#type_exec_table_result)
**Kind**: global function  

| Param | Type |
| --- | --- |
| from | [<code>type\_exec\_table\_result</code>](#type_exec_table_result) | 

<a name="noname_row_beautify"></a>

## noname\_row\_beautify(table, type_ext)
**Kind**: global function  

| Param | Type |
| --- | --- |
| table | [<code>type\_exec\_table\_result</code>](#type_exec_table_result) | 
| type_ext | <code>boolean</code> | 

<a name="find_disappeared_numbers"></a>

## find\_disappeared\_numbers(nums) ⇒ <code>Array.&lt;number&gt;</code>
**Kind**: global function  

| Param | Type |
| --- | --- |
| nums | <code>Array.&lt;number&gt;</code> | 

<a name="type_connection_config"></a>

## type\_connection\_config
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| server | <code>string</code> | MS SQL instance, for example - 'server\\instance1' |
| password | <code>string</code> | passrord for ms sql authentication or windows authentication |
| [login] | <code>string</code> | login for ms sql authentication, if empty - os authentication |
| database | <code>string</code> | name database for connect, default - 'tempdb' |
| [app_name] | <code>string</code> | app name, which will be visible in profiler |
| [connection_timeout] | <code>number</code> | connection timeout in milliseconds, default - 15000 |
| [execution_timeout] | <code>number</code> | execution timeout in milliseconds, default - 0 (infinity) |
| [encrypt_connection] | <code>boolean</code> | encrypt connection, default - false |
| [useUTC] | <code>boolean</code> | default - true |
| [action_allow] | <code>boolean</code> | use action subsystem, default - false |
| [again_connect_count] | <code>number</code> | count to try to connect to the server if the connection is unavailable, default - 0 (again try connect is disabled) |
| [again_connect_timeout] | <code>number</code> | timeout in milliseconds between attempts to connect to the server, default - 2000 |

<a name="type_action"></a>

## type\_action
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| key | <code>string</code> | action uniq key |
| sql_script | <code>string</code> |  |
| [sql_param] | <code>string</code> |  |
| [sql_param_note] | <code>string</code> |  |
| [sql_result] | <code>string</code> |  |
| [sql_lock] | <code>string</code> | one thread - name |
| [sql_lock_wait] | <code>number</code> | one thread - wait timeout in milliseconds (0 - no wait), default 0 |
| [sql_lock_message] | <code>string</code> | one thread - message if lock exists |
| [js_script] | <code>string</code> |  |
| title | <code>string</code> | action title |
| [note] | <code>string</code> | action title |
| [keys_next] | <code>string</code> | exec this actions after exec this action, example - 'key1;key2' |
| [correction_utc_mode] | <code>string</code> |  |
| [extented_property] | <code>Object</code> |  |

<a name="type_generate_class_with_exec_actions"></a>

## type\_generate\_class\_with\_exec\_actions
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [action_class_template] | <code>string</code> | 
| [class_name] | <code>string</code> | 

<a name="callback_empty"></a>

## callback\_empty : <code>function</code>
**Kind**: global typedef  
<a name="callback_error"></a>

## callback\_error : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Error</code> | 

<a name="type_exec_option"></a>

## type\_exec\_option
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [correction_utc] | [<code>type\_exec\_correction\_utc\_option</code>](#type_exec_correction_utc_option) |  |
| [lock] | [<code>type\_exec\_lock\_option</code>](#type_exec_lock_option) | one thread subsystem |
| [noname_row_beautify] | <code>boolean</code> | default - false |
| [chunk_rows] | <code>number</code> | return result by chunk, default - off (undefined) |
| [chunk_msec] | <code>number</code> | return result by chunk, default - off (undefined) |
| [check_database_after_exec] | <code>boolean</code> | return database name, which is active at the time the script is finished, default - false |
| [check_spid] | <code>boolean</code> | return spid, for (example) kill process, default - false |
| [check_print] | <code>boolean</code> | return PRINT and RAISERROR, default - false |
| [type_ext] | <code>boolean</code> | create properties "type_name", "type_title", "type_charlen" in schema |

<a name="type_exec_lock_option"></a>

## type\_exec\_lock\_option
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [key] | <code>string</code> | 
| [wait] | <code>number</code> | 
| [message] | <code>string</code> | 

<a name="type_exec_lock_result"></a>

## type\_exec\_lock\_result
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [key] | <code>string</code> | 
| [wait] | <code>number</code> | 
| [message] | <code>string</code> | 
| allow | <code>boolean</code> | 

<a name="type_exec_correction_utc_option"></a>

## type\_exec\_correction\_utc\_option
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [offset] | <code>number</code> | correct field with datetime type by this value (in minutes) |
| [mode] | <code>string</code> | which fields correct by utc offset, example - '{#all}{}{fdm;ldm}' - in first table correct all datetime fields, second table is missing, in the third table correct only two fields |

<a name="type_exec_correction_utc_result"></a>

## type\_exec\_correction\_utc\_result
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| [offset] | <code>number</code> | correct field with datetime type by this value (in minutes) |
| [mode] | <code>string</code> | which fields correct by utc offset, example - '{#all}{}{fdm;ldm}' - in first table correct all datetime fields, second table is missing, in the third table correct only two fields |

<a name="type_exec_error_result"></a>

## type\_exec\_error\_result
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| step | <code>number</code> | 
| error | <code>Error</code> | 
| point | <code>&#x27;connect&#x27;</code> \| <code>&#x27;sql&#x27;</code> \| <code>&#x27;kill&#x27;</code> \| <code>&#x27;action&#x27;</code> | 

<a name="type_exec_script_result"></a>

## type\_exec\_script\_result
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| [lock_on] | <code>string</code> | 
| [lock_off] | <code>string</code> | 
| queue | <code>Array.&lt;string&gt;</code> | 
| [name_server] | <code>string</code> | 
| [name_database] | <code>string</code> | 
| [name_database_after_exec] | <code>string</code> | 
| [spid] | <code>number</code> | 
| [spguid] | <code>string</code> | 
| get_plain_query | <code>function</code> | 

<a name="type_exec_table_result"></a>

## type\_exec\_table\_result
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| schema | <code>Object</code> | 
| step | <code>number</code> | 
| table_index | <code>number</code> | 
| rows | <code>Array.&lt;Object&gt;</code> | 

<a name="function_tables_to_xml_file1"></a>

## function\_tables\_to\_xml\_file1 : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Error</code> | 
| xml | <code>string</code> | 

<a name="function_tables_to_xml_file2"></a>

## function\_tables\_to\_xml\_file2 : <code>function</code>
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| [full_file_name] | <code>string</code> | //if empty, see xml as a string in callback |
| table_name_list | <code>Array.&lt;string&gt;</code> |  |
| callback | [<code>function\_tables\_to\_xml\_file1</code>](#function_tables_to_xml_file1) |  |

<a name="type_performance"></a>

## type\_performance
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| perfomance_exec_before | <code>number</code> | 
| perfomance_exec_after | <code>number</code> | 
| duration_exec_millisecond | <code>Array.&lt;number&gt;</code> | 

<a name="type_exec_result"></a>

## type\_exec\_result
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| handle_error | [<code>type\_exec\_error\_result</code>](#type_exec_error_result) | 
| [lock] | [<code>type\_exec\_lock\_result</code>](#type_exec_lock_result) | 
| [script] | [<code>type\_exec\_script\_result</code>](#type_exec_script_result) | 
| [correction_utc] | [<code>type\_exec\_correction\_utc\_result</code>](#type_exec_correction_utc_result) | 
| performance | [<code>type\_performance</code>](#type_performance) | 
| [tables] | [<code>Array.&lt;type\_exec\_table\_result&gt;</code>](#type_exec_table_result) | 
| [tables_to_xml] | [<code>function\_tables\_to\_xml\_file2</code>](#function_tables_to_xml_file2) | 

<a name="type_exec_runtime"></a>

## type\_exec\_runtime
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| type | <code>&#x27;spid&#x27;</code> \| <code>&#x27;print&#x27;</code> \| <code>&#x27;table&#x27;</code> | 
| [spid] | [<code>type\_exec\_spid</code>](#type_exec_spid) | 
| [print] | [<code>type\_exec\_print</code>](#type_exec_print) | 
| [table] | [<code>type\_exec\_table\_result</code>](#type_exec_table_result) | 

<a name="type_exec_spid"></a>

## type\_exec\_spid
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| spguid | <code>string</code> | 
| spid | <code>number</code> | 
| exists | <code>boolean</code> | 
| killed | <code>boolean</code> | 

<a name="type_exec_print"></a>

## type\_exec\_print
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| message | <code>string</code> | 
| procname | <code>string</code> | 
| is_error | <code>boolean</code> | 

<a name="callback_exec_part"></a>

## callback\_exec\_part : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Object</code> | 

<a name="callback_exec"></a>

## callback\_exec : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| result | [<code>type\_exec\_result</code>](#type_exec_result) | 
| runtime | [<code>type\_exec\_runtime</code>](#type_exec_runtime) | 

<a name="callback_exec_runtime"></a>

## callback\_exec\_runtime : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| runtime | [<code>type\_exec\_runtime</code>](#type_exec_runtime) | 

<a name="callback_print"></a>

## callback\_print : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| print | [<code>type\_exec\_print</code>](#type_exec_print) | 

<a name="callback_table"></a>

## callback\_table : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| table | [<code>type\_exec\_table\_result</code>](#type_exec_table_result) | 

<a name="callback_action"></a>

## callback\_action : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| result | [<code>type\_exec\_result</code>](#type_exec_result) | 
| named_tables | <code>Array.&lt;Object&gt;</code> | 
| warning | <code>Array.&lt;string&gt;</code> | 

<a name="callback_get_connection_pool"></a>

## callback\_get\_connection\_pool : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Error</code> | 
| connection_pool | <code>lib\_sql.ConnectionPool</code> | 

<a name="callback_connect"></a>

## callback\_connect : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Error</code> | 
| connection_pool | <code>lib\_sql.Request</code> | 

<a name="type_action_find"></a>

## type\_action\_find
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| key | <code>string</code> | 
| title | <code>string</code> | 
| sql_result_list | <code>Array.&lt;string&gt;</code> | 
| note | <code>string</code> | 
| error_list | <code>Array.&lt;string&gt;</code> | 

<a name="callback_get_newid"></a>

## callback\_get\_newid : <code>function</code>
**Kind**: global typedef  

| Param | Type |
| --- | --- |
| error | <code>Error</code> | 
| guid | <code>string</code> | 

